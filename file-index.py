import os
from string import Template

print_format = Template("[\"$file_path\", \"$file_name\"],")

dir_path = "docs"

# file_list = os.listdir(dir_path)


file_list = next(os.walk(dir_path))[2]
file_list.sort()

# print(file_list)


for file in file_list:
    file_path = "docs/" + file
    if os.path.isfile(file_path):
        with open(file_path, 'r') as f:
            if file == "README.md":
                file = "/"
            # else:
                # file = file[4:]
            file_title = f.readline().replace('#', '').replace("\n", '').strip()
            print(print_format.substitute(file_path=file, file_name=file_title))














