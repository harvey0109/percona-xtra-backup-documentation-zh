# 使用AppArmor

Linux安全模块通过AppArmor实现了强制访问控制（MAC）。Debian和Ubuntu系统默认安装AppArmor。AppArmor使用配置文件，定义应用程序需要哪些文件和权限。

Percona XtraBackup没有配置文件，不受AppArmor的限制。

有关常用AppArmor命令的列表，请参见https://www.percona.com/doc/percona-server/LATEST/security/apparmor.html

## 建立配置

从以下位置下载配置文件：

https://github.com/percona/percona-xtrabackup/tree/8.0/packaging/percona/apparmor/apparmor.d

以下配置文件部分应使用您的系统信息（例如备份目标目录的位置）进行更新。

```
# enable storing backups only in /backups directory
# /backups/** rwk,

# enable storing backups anywhere in caller user home directory
/@{HOME}/** rwk,


# enable storing backups only in /backups directory
# /backups/** rwk,

# enable storing backups anywhere in caller user home directory
/@{HOME}/** rwk,
}

# enable storing backups only in /backups directory
# /backups/** rwk,

# enable storing backups anywhere in caller user home directory
/@{HOME}/** rwk,
}
```

移动更新的文件：

```
$ sudo mv usr.sbin.xtrabackup /etc/apparmor.d/
```

使用以下命令安装配置文件：

```
$ sudo apparmor_parser -r -T -W /etc/apparmor.d/usr.sbin.xtrabackup
```

照常运行备份。

无需执行其他与AppArmor相关的操作即可还原备份。



