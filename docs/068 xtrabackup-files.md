# Index of files created by Percona XtraBackup



- Information related to the backup and the server

>- <dl class="first docutils">
<dt><code class="file docutils literal"><span class="pre">backup-my.cnf</span></code></dt>
<dd><p class="first last">This file contains information to start the mini instance of InnoDB
during the <a class="reference internal" href="xtrabackup_bin/xbk_option_reference.html#cmdoption-prepare"><code class="xref std std-option docutils literal"><span class="pre">--prepare</span></code></a>. This is <strong>NOT</strong> a backup of
original <code class="file docutils literal"><span class="pre">my.cnf</span></code>. The InnoDB configuration is read from the file
<code class="file docutils literal"><span class="pre">backup-my.cnf</span></code> created by <strong class="program">xtrabackup</strong> when the backup was
made. <a class="reference internal" href="xtrabackup_bin/xbk_option_reference.html#cmdoption-prepare"><code class="xref std std-option docutils literal"><span class="pre">--prepare</span></code></a> uses InnoDB configuration from
<code class="docutils literal"><span class="pre">backup-my.cnf</span></code> by default, or from
<a class="reference internal" href="xtrabackup_bin/xbk_option_reference.html#cmdoption-defaults-file"><code class="xref std std-option docutils literal"><span class="pre">--defaults-file</span></code></a>, if specified. InnoDB
configuration in this context means server variables that affect data
format, i.e. <code class="docutils literal"><span class="pre">innodb_page_size</span></code> option,
<code class="docutils literal"><span class="pre">innodb_log_block_size</span></code>, etc. Location-related variables, like
<code class="docutils literal"><span class="pre">innodb_log_group_home_dir</span></code> or <code class="docutils literal"><span class="pre">innodb_data_file_path</span></code>
are always ignored by <a class="reference internal" href="xtrabackup_bin/xbk_option_reference.html#cmdoption-prepare"><code class="xref std std-option docutils literal"><span class="pre">--prepare</span></code></a>, so preparing
a backup always works with data files from the backup directory, rather
than any external ones.</p>
</dd>
</dl>
- <dl class="first docutils">
<dt><code class="file docutils literal"><span class="pre">xtrabackup_checkpoints</span></code></dt>
<dd><p class="first">The type of the backup (e.g. full or incremental), its state (e.g.
prepared) and the <a class="reference internal" href="glossary.html#term-lsn"><span class="xref std std-term">LSN</span></a> range contained in it. This information is used
for incremental backups.
Example of the <code class="file docutils literal"><span class="pre">xtrabackup_checkpoints</span></code> after taking a full
backup:</p>
<div class="highlight-text"><div class="highlight"><pre><span></span>backup_type = full-backuped
from_lsn = 0
to_lsn = 15188961605
last_lsn = 15188961605
</pre></div>
</div>
<p>Example of the <code class="file docutils literal"><span class="pre">xtrabackup_checkpoints</span></code> after taking an incremental
backup:</p>
<div class="last highlight-text"><div class="highlight"><pre><span></span>backup_type = incremental
from_lsn = 15188961605
to_lsn = 15189350111
last_lsn = 15189350111
</pre></div>
</div>
</dd>
</dl>
- <dl class="first docutils">
<dt><code class="file docutils literal"><span class="pre">xtrabackup_binlog_info</span></code></dt>
<dd><p class="first last">The binary log file used by the server and its position at the moment of
the backup. Result of the <strong class="command">SHOW MASTER STATUS</strong>.</p>
</dd>
</dl>
- <dl class="first docutils">
<dt><code class="file docutils literal"><span class="pre">xtrabackup_binlog_pos_innodb</span></code></dt>
<dd><p class="first last">The binary log file and its current position for <em>InnoDB</em> or <em>XtraDB</em>
tables.</p>
</dd>
</dl>
- <dl class="first docutils">
<dt><code class="file docutils literal"><span class="pre">xtrabackup_binary</span></code></dt>
<dd><p class="first last">The <strong class="program">xtrabackup</strong> binary used in the process.</p>
</dd>
</dl>
- <dl class="first docutils">
<dt><code class="file docutils literal"><span class="pre">xtrabackup_logfile</span></code></dt>
<dd><p class="first last">Contains data needed for running the: <a class="reference internal" href="xtrabackup_bin/xbk_option_reference.html#cmdoption-prepare"><code class="xref std std-option docutils literal"><span class="pre">--prepare</span></code></a>.
The bigger this file is the <a class="reference internal" href="xtrabackup_bin/xbk_option_reference.html#cmdoption-prepare"><code class="xref std std-option docutils literal"><span class="pre">--prepare</span></code></a> process
will take longer to finish.</p>
</dd>
</dl>
- <dl class="first docutils">
<dt><code class="file docutils literal"><span class="pre">&lt;table_name&gt;.delta.meta</span></code></dt>
<dd><p class="first">This file is going to be created when performing the incremental backup.
It contains the per-table delta metadata: page size, size of compressed
page (if the value is 0 it means the tablespace isn’t compressed) and
space id. Example of this file could looks like this:</p>
<div class="last highlight-text"><div class="highlight"><pre><span></span>page_size = 16384
zip_size = 0
space_id = 0
</pre></div>
</div>
</dd>
</dl>
- Information related to the replication environment (if using the
[`<span class="pre">--slave-info</span>`](xtrabackup_bin/xbk_option_reference.html#cmdoption-slave-info) option):

>- <dl class="first docutils">
<dt><code class="file docutils literal"><span class="pre">xtrabackup_slave_info</span></code></dt>
<dd>The <code class="docutils literal"><span class="pre">CHANGE</span> <span class="pre">MASTER</span></code> statement needed for setting up a replica.</dd>
</dl>
- Information related to the *Galera* and *Percona XtraDB Cluster* (if using
the [`<span class="pre">--galera-info</span>`](xtrabackup_bin/xbk_option_reference.html#cmdoption-galera-info) option):

>- <dl class="first docutils">
<dt><code class="file docutils literal"><span class="pre">xtrabackup_galera_info</span></code></dt>
<dd>Contains the values of <code class="docutils literal"><span class="pre">wsrep_local_state_uuid</span></code> and
<code class="docutils literal"><span class="pre">wsrep_last_committed</span></code> status variables</dd>
</dl>