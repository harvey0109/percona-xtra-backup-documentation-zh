# 进行压缩备份

为了做一个压缩的备份，请使用--compress选项和--backup和--target-dir选项。默认情况下，--compress使用quicklz压缩算法。

```
$ xtrabackup --backup --compress --target-dir=/data/backup
```

您还可以通过将：option：-compress设置为lz4来使用lz4压缩算法。

```
$ xtrabackup --backup --compress=lz4 --target-dir=/data/backup
```

如果你想加快压缩速度，你可以使用并行压缩，可以用 --compress-reads 选项启用。下面的例子使用四个线程进行压缩。

```
$ xtrabackup --backup --compress --compress-threads=4 --target-dir=/data/backup
```

输出应如下所示

```
...

[01] Compressing ./imdb/comp_cast_type.ibd to /data/backup/imdb/comp_cast_type.ibd.qp
[01]        ...done
...
130801 11:50:24  xtrabackup: completed OK
```

## 准备备份

在准备备份之前，你需要用qpress（可从Percona软件仓库获得）解压所有文件。你可以使用下面的单行代码来解压所有的文件。

```
$ for bf in `find . -iname "*\.qp"`; do qpress -d $bf $(dirname $bf) && rm $bf; done
```

如果你使用了lz4压缩算法，请改变这个脚本来搜索`*.lz4`文件。

```
$ for bf in `find . -iname "*\.lz4"`; do lz4 -d $bf $(dirname $bf) && rm $bf; done
```

您可以使用--decompress选项解压缩备份：

```
$ xtrabackup --decompress --target-dir=/data/backup/
```

See also

- What is required to use the –decompress option effectively?

  [`--decompress`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-decompress)

当文件被解压缩后，你可以用--apply-log-only选项准备备份。

```
$ xtrabackup --apply-log-only --target-dir=/data/backup/
```

您应该检查确认消息：

```
130802 02:51:02  xtrabackup: completed OK!
```

现在，/data/backup/中的文件已经准备好被服务器使用了。

注意事项
Percona XtraBackup不会自动删除压缩的文件。为了清理备份目录，用户应该删除`*.qp`文件。

## 恢复备份

一旦备份准备好了，你就可以使用--拷贝--备份来恢复备份。

```
$ xtrabackup --copy-back --target-dir=/data/backup/
```

这会将准备好的数据复制回my.cnf中datadir变量定义的原始位置。

在确认信息之后，你应该在复制数据之后检查文件的权限。

```
130802 02:58:44  xtrabackup: completed OK!
```

你可能需要调整文件的权限。下面的例子演示了如何通过使用chown进行递归操作。

```
$ chown -R mysql:mysql /var/lib/mysql
```

现在，你的数据目录包含了恢复后的数据。你已经准备好启动服务器了。

