# 在Debian和Ubuntu上安装Percona XtraBackup

可从Percona XtraBackup软件存储库和[下载页面](https://www.percona.com/downloads/Percona-XtraBackup-LATEST/)获得即用型软件包。


Percona软件和平台生命周期中介绍了有关受支持的平台，产品和版本的特定信息。


## 每个DEB包中都有什么？

`percona-xtrabackup-80` 包含最新的Percona XtraBackup GA二进制文件和关联的文件。

`percona-xtrabackup-dbg-80`  软件包对percona-xtrabackup-80中二进制文件的调试

`percona-xtrabackup-test-80` 包含Percona XtraBackup的测试套件。

`percona-xtrabackup`  包含旧版本的Percona XtraBackup



## 通过percona版本安装Percona XtraBackup

与许多其他Percona产品一样，Percona XtraBackup是通过percona-release软件包配置工具安装的。

1. 从Percona网站下载用于percona的deb软件包，以释放存储库软件包：

```bash
wget https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb
```

1. 使用dpkg安装下载的软件包。为此，请以超级用户身份或使用sudo运行以下命令

```bash
dpkg -i percona-release_latest.$(lsb_release -sc)_all.deb
```

安装此软件包后，应添加Percona存储库。您可以在 `/etc/apt/sources.list.d/percona-release.list`文件中检查存储库设置。

1. 启用存储库：`percona-release enable-only tools release`

如果打算将Percona XtraBackup与上游MySQL Server结合使用，则只需启用工具存储库：percona-release仅启用工具。

1. 记住要更新本地缓存：`apt-get update`

1. 之后，您可以安装percona-xtrabackup-80软件包

```bash
sudo apt-get install percona-xtrabackup-80
```

1. 为了进行压缩备份，请安装qpress软件包：

```bash
sudo apt-get install qpress
```

参见：

[压缩备份](https://www.percona.com/doc/percona-xtrabackup/LATEST/backup_scenarios/compressed_backup.html#compressed-backup)

## 固定版本

在某些情况下，您可能需要“固定”选定的软件包，以避免从发行版本存储库升级。您需要制作一个新文件`/etc/apt/preferences.d/00percona.pref`，并在其中添加以下几行：

```
Package: *
Pin: release o=Percona Development Team
Pin-Priority: 1001
```

有关固定的更多信息，您可以查看官方的[debian Wiki](http://wiki.debian.org/AptPreferences)。

## 使用下载的Deb软件包安装Percona XtraBackup

从[下载页面](https://www.percona.com/downloads/XtraBackup/)下载适用于您的体系结构的所需系列的软件包。以下示例将下载Debian 8.0的Percona XtraBackup 8.0.4-1发行包：

```bash
wget https://www.percona.com/downloads/XtraBackup/Percona-XtraBackup-8.0.4/binary/debian/stretch/x86_64/percona-xtrabackup-80_8.0.4-1.stretch_amd64.deb
```

现在，您可以运行以下命令来安装Percona XtraBackup：

```bash
sudo dpkg -i percona-xtrabackup-80_0.4-1.stretch_amd64.deb
```

::: tip 笔记

像这样手动安装软件包时，您需要确保解决所有依赖性并自行安装丢失的软件包。
:::

## 卸载Percona XtraBackup

要卸载Percona XtraBackup，您需要删除所有已安装的软件包。

卸载软件包

```bash
sudo apt-get remove percona-xtrabackup-80
```
