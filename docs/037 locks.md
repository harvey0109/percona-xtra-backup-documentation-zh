# lock-ddl-per-table选项改进

为了阻止实例上的DDL语句，Percona服务器实现了LOCK TABLES FOR BACKUP。Percona XtraBackup在备份的过程中使用这个锁。这个锁并不影响DML语句。

Percona XtraBackup还实现了--lock-ddl-per-table，它通过使用元数据锁（MDL）来阻止DDL语句。

下面的程序描述了使用-lock-ddl-per-table时的简化备份操作。

- 解析并复制检查点标记后的所有重做日志
- 分叉专用线程以继续关注新的重做日志条目
- 列出复制所需的表空间
- 遍历列表。每个列出的表空间都执行以下步骤：

a. 查询INFORMATION_SCHEMA.INNODB_TABLES，找到哪些表属于表空间ID，如果有一个共享的表空间，就对底层的表进行MDL。

复制表空间.ibd文件。

备份过程可能会遇到一个由批量加载操作产生的重做日志事件，它通知备份工具，数据文件的变化已经从重做日志中省略。这个事件是一个MLOG_INDEX_LOAD。如果这个事件被重做跟踪线程发现，备份继续进行，并假定备份是安全的，因为MDL保护已经复制的表空间，而且MLOG_INDEX_LOAD事件是针对没有复制的表空间。

这些假设可能不正确，可能导致不一致的备份。

## --lock-ddl-per-table重新设计

在Percona XtraBackup 8.0.22-15.0版本中实施，--lock-ddl-per-table已经被重新设计，以减少不一致的备份。下面的程序重新排列了步骤。

- 备份开始时获取的MDL锁
- 扫描重做日志。如果在备份开始前发生了CREATE INDEX语句，可能会记录一个MLOG_INDEX_LOAD事件。
  在备份开始前发生了CREATE INDEX语句。这时，备份过程是安全的，可以解析并接受该事件。
- 第一次扫描完成后，以下重做日志线程为
  发起。如果找到MLOG_INDEX_LOAD事件，则该线程将停止备份过程。
- 收集表空间列表以进行复制
- 复制.ibd文件。

## 其他改进

添加了以下改进：

- 如果.ibd文件属于临时表，则执行SELECT查询
  被跳过。
- 对于全文索引，在基表上获取一个MDL。
- 获取MDL的SELECT查询不会检索任何数据。

警告
在MySQL 8.0的Percona Server中，不建议使用lock-ddl-per-table变量。使用--lock-ddl代替此变量。

