# 安装Percona XtraBackup 8.0

本页提供有关如何安装Percona XtraBackup的信息。提供以下选项：

- 从存储库安装Percona XtraBackup（推荐）
- 从二进制Tarball安装Percona XtraBackup
- 从下载的rpm或apt软件包安装Percona XtraBackup
- 从源代码编译和安装

在安装之前，您可能需要阅读[Percona XtraBackup 8.0](https://www.percona.com/doc/percona-xtrabackup/LATEST/release-notes.html)发行说明。


## 从存储库安装Percona XtraBackup

Percona为yum（用于Red Hat，CentOS和Amazon Linux AMI的RPM软件包）和apt（用于Ubuntu和Debian的.deb软件包）提供存储库，以用于软件（例如，用于MySQL的Percona Server，Percona XtraBackup和Percona Toolkit）。这样，您就可以通过操作系统的程序包管理器轻松安装和更新软件及其依赖项。这是安装Percona XtraBackup的推荐方法。

以下指南描述了将官方的Percona存储库用于.deb和.rpm软件包的安装过程。

- [在Debian和Ubuntu上安装Percona XtraBackup](apt_repo.html)
- [在Red Hat Enterprise Linux和CentOS上安装Percona XtraBackup](005-yum_repo.html)

::: tip 笔记

为了从早期版本的数据库服务器进行实验性迁移，您将需要使用XtraBackup 2.4进行备份和还原，然后使用MySQL 8.0.x中的mysql_upgrade进行备份和还原。

参见 

Percona XtraBackup 2.4和8.0中MySQL版本支持的MySQL和Percona Server
[About Percona XtraBackup](https://www.percona.com/doc/percona-xtrabackup/LATEST/intro.html#intro)
:::


## 从二进制Tarball安装Percona XtraBackup

二进制tarball可供[下载](https://www.percona.com/downloads/Percona-XtraBackup-LATEST/)和安装。选择Percona XtraBackup 8.0版本，软件或操作系统以及要安装的tarball类型。

下表列出了 Linux - Generic 中可用的tarball类型。两种类型都支持所有发行版。

| Type    | Name                                                         | Description                                                  |
| :------ | :----------------------------------------------------------- | :----------------------------------------------------------- |
| 完整    | `percona-xtrabackup-<version number>-Linux.x86_64.glibc2.12.tar.gz` | 包含二进制文件，库，测试文件和调试符号  |
| 最小的 | `percona-xtrabackup-<version number>-Linux.x86_64.glibc2.12-minimal.tar.gz` | 包含二进制文件和库，但不包含测试文件或调试符号 |


选择其他软件，例如Ubuntu 20.04（Focal Fossa），可为该操作系统提供压缩包。您可以一起下载软件包，也可以单独下载。

以下链接是下载适用于Linux / Generic的完整tarball的示例：

```bash
wget https://downloads.percona.com/downloads/Percona-XtraBackup-LATEST/Percona-XtraBackup-8.0.23-16/binary/tarball/percona-xtrabackup-8.0.23-16-Linux-x86_64.glibc2.17.tar.gz
```

## 从源代码编译和安装

Percona XtraBackup是开源的，该代码在[Github](https://github.com/percona/percona-xtrabackup)上可用。以下指南从源代码描述了编译和安装过程。

- [从源代码编译和安装](https://www.percona.com/doc/percona-xtrabackup/LATEST/installation/compiling_xtrabackup.html)

## 在Docker容器中运行Percona XtraBackup


Percona XtraBackup 8.0的Docker映像在Docker Hub上公开托管，位于[https://hub.docker.com/r/percona/percona-xtradb-cluster/](https://hub.docker.com/r/percona/percona-xtradb-cluster/)


有关如何使用Docker的更多信息，请参阅 [Docker Docs](https://docs.docker.com/) 。


::: tip 笔记

确保您使用的是最新版本的Docker。通过apt和yum提供的代码可能已过时并导致错误。
::: 

::: tip 笔记
默认情况下，如果本地不可用，则Docker将从Docker Hub中拉取映像。

:::
以下过程描述了...