# 使服务器能够通过TCP / IP进行通信

大多数Linux发行版在其MySQL或Percona服务器软件包中没有默认启用接受来自外部的TCP/IP连接。

您可以在外壳上使用netstat进行检查：

```
$ netstat -lnp | grep mysql
tcp         0        0 0.0.0.0:3306 0.0.0.0:* LISTEN 2480/mysqld
unix 2 [ ACC ] STREAM LISTENING 8101 2480/mysqld /tmp/mysql.sock
```

您应该检查两件事：

- 有一行以tcp开头（服务器确实接受TCP连接），并且第一个地址（本例中为0.0.0.0:3306）与127.0.0.1:3306不同（绑定地址不是localhost的）。
- 第一个地址（本例中为0.0.0.0:3306）与127.0.0.1:3306不同（绑定的地址不是localhost的）。

在第一种情况下，首先要看的是my.cnf文件。如果你发现了 skip-networking 选项，把它注释掉或直接删除。还要检查一下，如果变量bind_address被设置了，那么它不应该被设置为localhost的，而应该是主机的IP。然后重新启动MySQL服务器，再次用netstat检查。如果你做的改变没有效果，那么你应该看看你的发行版的启动脚本（如rc.mysqld）。你应该注释掉像--skip-networking这样的标志和/或改变bind-address。

在你让服务器正确监听远程TCP连接之后，最后要做的事情是检查端口（默认为3306）是否确实打开。检查你的防火墙配置（iptables -L），你是否允许该端口的远程主机（在/etc/hosts.allow）。

然后我们就完成了! 我们有一个正在运行的MySQL服务器，它能够通过TCP/IP与世界通信。