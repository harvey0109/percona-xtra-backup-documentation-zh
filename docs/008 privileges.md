# 需要连接和特权

在创建备份，某些情况下进行准备以及还原时，Percona XtraBackup需要能够连接到数据库服务器并在服务器和datadir上执行操作。为此，必须满足其执行上的特权和许可要求。

特权是指允许系统用户在数据库服务器中执行的操作。它们在数据库服务器上设置，仅适用于数据库服务器中的用户。

权限是指那些允许用户在系统上执行操作的权限，例如在特定目录上进行读取，写入或执行或启动/停止系统服务。它们是在系统级别设置的，仅适用于系统用户。

使用xtrabackup时，涉及两个参与者：调用程序的用户-系统用户-和在数据库服务器中执行操作的用户-数据库用户。请注意，即使这些用户使用相同的用户名，他们在不同的地方也是不同的用户。

本文档中所有xtrabackup调用均假定系统用户具有适当的权限，并且您要提供连接数据库服务器的相关选项（除了要执行的操作的选项之外），并且数据库用户具有足够的特权。

## 连接到服务器

用于连接服务器的数据库用户及其密码由`--user`和`--password`选项指定：
```
$ xtrabackup --user=DVADER --password=14MY0URF4TH3R --backup \
--target-dir=/data/bkps/
```
如果您不使用`--user`选项，则Percona XtraBackup将假定数据库用户的名称是执行该数据库的系统用户。

## 其他连接选项

根据您的系统，您可能需要指定以下一个或多个选项才能连接到服务器：


| Option  | Description                                                  |
| :------ | :----------------------------------------------------------- |
| –port   | 使用TCP / IP连接到数据库服务器时使用的端口。 |
| –socket | 连接到本地数据库时使用的套接字。     |
| –host   | 使用TCP / IP连接到数据库服务器时要使用的主机。 |

这些选项无需更改即可传递给mysql子进程，有关详细信息，请参见`mysql --help`。

::: tip 笔记

如果有多个服务器实例，则必须指定正确的连接参数（端口，套接字，主机），以便xtrabackup与正确的服务器通信。
:::


## 所需的权限和特权

连接到服务器后，要执行备份，您将需要在服务器数据目录(datadir)中文件系统级别的读取和执行权限。

数据库用户需要备份表/数据库的以下特权：

- `RELOAD`和`LOCK TABLES`（除非指定了--no-lock选项），以便在开始复制文件之前运行`FLUSH TABLES WITH READ LOCK `和`FLUSH ENGINE LOGS`，并且在使用 `Backup Lock` 时需要此特权

- `BACKUP_ADMIN` 需要特权来查询 `performance_schema.log_status` 表，并运行`LOCK INSTANCE FOR BACKUP`，`LOCK BINLOG FOR BACKUP`或`LOCK TABLES FOR BACKUP`。

- `REPLICATION CLIENT` 特权, 为了获得二进制日志位置

- `CREATE TABLESPACE` 特权, 以便导入表（请参阅还原单个表）

- `PROCESS` 特权, 为了运行`SHOW ENGINE INNODB STATUS`（这是强制性的）的过程，并且有选择地查看服务器上正在运行的所有线程（请参阅`FLUSH TABLES WITH READ LOCK`），

- `SUPER` 特权, 为了在复制环境中启动/停止复制线程，请使用XtraDB Changed Page Tracking进行增量备份并处理`FLUSH TABLES WITH READ LOCK,`，

- `CREATE` 特权, 为了创建`PERCONA_SCHEMA.xtrabackup_history`数据库和表

- `INSERT` 特权，以便将历史记录添加到`PERCONA_SCHEMA.xtrabackup_history`表中，

- `SELECT` 为了使用`--incremental-history-name`或`--incremental-history-uuid`，以便该功能在`PERCONA_SCHEMA.xtrabackup_history`表中查找`innodb_to_lsn`值。


有关何时使用它们的说明，可以在[Percona XtraBackup的工作原理](https://www.percona.com/doc/percona-xtrabackup/LATEST/how_xtrabackup_works.html#how-xtrabackup-works)中找到。


一个以完全备份所需的最低特权创建数据库用户的SQL示例为：

```sql
CREATE USER 'bkpuser'@'localhost' IDENTIFIED BY 's3cr%T';
GRANT BACKUP_ADMIN, PROCESS, RELOAD, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'bkpuser'@'localhost';
GRANT SELECT ON performance_schema.log_status TO 'bkpuser'@'localhost';
FLUSH PRIVILEGES;
```
