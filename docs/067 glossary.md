# Glossary


<dl class="glossary docutils">
<dt id="term-uuid">UUID</dt>
<dd><p class="first">Universally Unique IDentifier which uniquely identifies the state and the
sequence of changes node undergoes. 128-bit UUID is a classic DCE UUID
Version 1 (based on current time and MAC address). Although in theory this
UUID could be generated based on the real MAC-address, in the Galera it is
always (without exception) based on the generated pseudo-random addresses
(“locally administered” bit in the node address (in the UUID structure) is
always equal to unity).</p>
<p>Complete structure of the 128-bit UUID field and explanation for its
generation are as follows:</p>
<table border="1" class="last docutils">
<colgroup>
<col width="7%"/>
<col width="6%"/>
<col width="10%"/>
<col width="77%"/>
</colgroup>
<thead valign="bottom">
<tr class="row-odd"><th class="head">From</th>
<th class="head">To</th>
<th class="head">Length</th>
<th class="head">Content</th>
</tr>
</thead>
<tbody valign="top">
<tr class="row-even"><td>0</td>
<td>31</td>
<td>32</td>
<td>Bits 0-31 of Coordinated Universal Time (UTC) as a
count of 100-nanosecond intervals since 00:00:00.00,
15 October 1582, encoded as big-endian 32-bit number.</td>
</tr>
<tr class="row-odd"><td>32</td>
<td>47</td>
<td>16</td>
<td>Bits 32-47 of UTC as a count of 100-nanosecond
intervals since 00:00:00.00, 15 October 1582, encoded
as big-endian 16-bit number.</td>
</tr>
<tr class="row-even"><td>48</td>
<td>59</td>
<td>12</td>
<td>Bits 48-59 of UTC as a count of 100-nanosecond
intervals since 00:00:00.00, 15 October 1582, encoded
as big-endian 16-bit number.</td>
</tr>
<tr class="row-odd"><td>60</td>
<td>63</td>
<td>4</td>
<td>UUID version number: always equal to 1 (DCE UUID).</td>
</tr>
<tr class="row-even"><td>64</td>
<td>69</td>
<td>6</td>
<td>most-significants bits of random number, which
generated from the server process PID and Coordinated
Universal Time (UTC) as a count of 100-nanosecond
intervals since 00:00:00.00, 15 October 1582.</td>
</tr>
<tr class="row-odd"><td>70</td>
<td>71</td>
<td>2</td>
<td>UID variant: always equal to binary 10 (DCE variant).</td>
</tr>
<tr class="row-even"><td>72</td>
<td>79</td>
<td>8</td>
<td>8 least-significant bits of  random number, which
generated from the server process PID and Coordinated
Universal Time (UTC) as a count of 100-nanosecond
intervals since 00:00:00.00, 15 October 1582.</td>
</tr>
<tr class="row-odd"><td>80</td>
<td>80</td>
<td>1</td>
<td>Random bit (“unique node identifier”).</td>
</tr>
<tr class="row-even"><td>81</td>
<td>81</td>
<td>1</td>
<td>Always equal to the one (“locally administered MAC
address”).</td>
</tr>
<tr class="row-odd"><td>82</td>
<td>127</td>
<td>46</td>
<td>Random bits (“unique node identifier”): readed from
the <code class="file docutils literal"><span class="pre">/dev/urandom</span></code> or (if <code class="file docutils literal"><span class="pre">/dev/urandom</span></code>
is unavailable) generated based on the server process
PID, current time and bits of the default “zero node
identifier” (entropy data).</td>
</tr>
</tbody>
</table>
</dd>
<dt id="term-lsn">LSN</dt>
<dd>Each InnoDB page (usually 16kb in size) contains a log sequence number, or
LSN. The LSN is the system version number for the entire database. Each
page’s LSN shows how recently it was changed.</dd>
<dt id="term-innodb-file-per-table">innodb_file_per_table</dt>
<dd><p class="first">By default, all InnoDB tables and indexes are stored in the system
tablespace on one file. This option causes the server to create one
tablespace file per table. To enable it, set it on your configuration file,</p>
<blockquote class="last">
<div><div class="highlight-text"><div class="highlight"><pre><span></span>[mysqld]
innodb_file_per_table
</pre></div>
</div>
<p>or start the server with <code class="docutils literal"><span class="pre">--innodb_file_per_table</span></code>.</p>
</div></blockquote>
</dd>
<dt id="term-innodb-expand-import">innodb_expand_import</dt>
<dd><p class="first">This feature of <em>Percona Server for MySQL</em> implements the ability to import
arbitrary <a class="reference internal" href="#term-ibd"><span class="xref std std-term">.ibd</span></a> files exported using the <em>Percona XtraBackup</em>
<a class="reference internal" href="xtrabackup_bin/xbk_option_reference.html#cmdoption-export"><code class="xref std std-option docutils literal"><span class="pre">--export</span></code></a> option.</p>
<p class="last">See the <a class="reference external" href="http://www.percona.com/doc/percona-server/5.5/management/innodb_expand_import.html">the full documentation</a>
for more information.</p>
</dd>
<dt id="term-innodb-data-home-dir">innodb_data_home_dir</dt>
<dd><p class="first">The directory (relative to <a class="reference internal" href="#term-datadir"><span class="xref std std-term">datadir</span></a>) where the database server
stores the files in a shared tablespace setup. This option does not affect
the location of <a class="reference internal" href="#term-innodb-file-per-table"><span class="xref std std-term">innodb_file_per_table</span></a>. For example:</p>
<blockquote class="last">
<div><div class="highlight-text"><div class="highlight"><pre><span></span>[mysqld]
innodb_data_home_dir = ./
</pre></div>
</div>
</div></blockquote>
</dd>
<dt id="term-innodb-data-file-path">innodb_data_file_path</dt>
<dd><p class="first">Specifies the names, sizes and location of shared tablespace files:</p>
<blockquote class="last">
<div><div class="highlight-text"><div class="highlight"><pre><span></span>[mysqld]
innodb_data_file_path=ibdata1:50M;ibdata2:50M:autoextend
</pre></div>
</div>
</div></blockquote>
</dd>
<dt id="term-innodb-log-group-home-dir">innodb_log_group_home_dir</dt>
<dd><p class="first">Specifies the location of the <em>InnoDB</em> log files:</p>
<blockquote class="last">
<div><div class="highlight-text"><div class="highlight"><pre><span></span>[mysqld]
innodb_log_group_home=/var/lib/mysql
</pre></div>
</div>
</div></blockquote>
</dd>
<dt id="term-innodb-buffer-pool-size">innodb_buffer_pool_size</dt>
<dd><p class="first">The size in bytes of the memory buffer to cache data and indexes of
<em>InnoDB</em>‘s tables. This aims to reduce disk access to provide better
performance. By default:</p>
<blockquote class="last">
<div><div class="highlight-text"><div class="highlight"><pre><span></span>[mysqld]
innodb_buffer_pool_size=8MB
</pre></div>
</div>
</div></blockquote>
</dd>
<dt id="term-innodb">InnoDB</dt>
<dd>Storage engine which provides ACID-compliant transactions and foreign
key support, among others improvements over <a class="reference internal" href="#term-myisam"><span class="xref std std-term">MyISAM</span></a>. It is the
default engine for <em>MySQL</em> as of the 8.0 series.</dd>
<dt id="term-myisam">MyISAM</dt>
<dd>Previous default storage engine for <em>MySQL</em> for versions prior to 5.5. It
doesn’t fully support transactions but in some scenarios may be faster
than <a class="reference internal" href="#term-innodb"><span class="xref std std-term">InnoDB</span></a>. Each table is stored on disk in 3 files:
<a class="reference internal" href="#term-frm"><span class="xref std std-term">.frm</span></a>, <a class="reference internal" href="#term-myd"><span class="xref std std-term">.MYD</span></a>, <a class="reference internal" href="#term-myi"><span class="xref std std-term">.MYI</span></a>.</dd>
<dt id="term-xtradb">XtraDB</dt>
<dd><em>Percona XtraDB</em> is an enhanced version of the InnoDB storage engine,
designed to better scale on modern hardware, and including a variety of
other features useful in high performance environments. It is fully
backwards compatible, and so can be used as a drop-in replacement for
standard InnoDB. More information <a class="reference external" href="https://www.percona.com/doc/percona-server/5.6/percona_xtradb.html">here</a>.</dd>
<dt id="term-my-cnf">my.cnf</dt>
<dd>This file refers to the database server’s main configuration file. Most
Linux distributions place it as <code class="file docutils literal"><span class="pre">/etc/mysql/my.cnf</span></code> or
<code class="file docutils literal"><span class="pre">/etc/my.cnf</span></code>, but the location and name depends on the particular
installation. Note that this is not the only way of configuring the
server, some systems does not have one even and rely on the command
options to start the server and its defaults values.</dd>
<dt id="term-datadir">datadir</dt>
<dd>The directory in which the database server stores its databases. Most Linux
distribution use <code class="file docutils literal"><span class="pre">/var/lib/mysql</span></code> by default.</dd>
<dt id="term-xbcrypt">xbcrypt</dt>
<dd>To support encryption and decryption of the backups, a new tool xbcrypt
was introduced to <em>Percona XtraBackup</em>. This utility has been modeled
after The xbstream binary to perform encryption and decryption outside of
<em>Percona XtraBackup</em>.</dd>
<dt id="term-xbstream">xbstream</dt>
<dd><dl class="first last docutils">
<dt>To support simultaneous compression and streaming, <em>Percona XtraBackup</em> uses the</dt>
<dd>xbstream format. For more information see <a class="reference internal" href="xtrabackup_bin/xbk_option_reference.html#cmdoption-stream"><code class="xref std std-option docutils literal"><span class="pre">--stream</span></code></a></dd>
</dl>
</dd>
<dt id="term-ibdata">ibdata</dt>
<dd>Default prefix for tablespace files, e.g. <code class="file docutils literal"><span class="pre">ibdata1</span></code> is a 10MB
auto-extensible file that <em>MySQL</em> creates for the shared tablespace by
default.</dd>
<dt id="term-frm">.frm</dt>
<dd>For each table, the server will create a file with the <code class="docutils literal"><span class="pre">.frm</span></code> extension
containing the table definition (for all storage engines).</dd>
<dt id="term-ibd">.ibd</dt>
<dd>On a multiple tablespace setup (<a class="reference internal" href="#term-innodb-file-per-table"><span class="xref std std-term">innodb_file_per_table</span></a> enabled),
<em>MySQL</em> will store each newly created table on a file with a <code class="docutils literal"><span class="pre">.ibd</span></code>
extension.</dd>
<dt id="term-myd">.MYD</dt>
<dd>Each <em>MyISAM</em> table has <code class="docutils literal"><span class="pre">.MYD</span></code> (MYData) file which contains the data on
it.</dd>
<dt id="term-myi">.MYI</dt>
<dd>Each <em>MyISAM</em> table has <code class="docutils literal"><span class="pre">.MYI</span></code> (MYIndex) file which contains the table’s
indexes.</dd>
<dt id="term-exp">.exp</dt>
<dd>Files with the <code class="docutils literal"><span class="pre">.exp</span></code> extension are created by <em>Percona XtraBackup</em> per
each <em>InnoDB</em> tablespace when the <a class="reference internal" href="xtrabackup_bin/xbk_option_reference.html#cmdoption-export"><code class="xref std std-option docutils literal"><span class="pre">--export</span></code></a> option is
used on prepare. These files can be used to import those tablespaces on
<em>Percona Server for MySQL</em> 5.5 or lower versions, see <a class="reference internal" href="xtrabackup_bin/restoring_individual_tables.html"><span class="doc">restoring individual
tables</span></a>”.</dd>
<dt id="term-mrg">.MRG</dt>
<dd>Each table using the <strong class="program">MERGE</strong> storage engine, besides of a
<a class="reference internal" href="#term-frm"><span class="xref std std-term">.frm</span></a> file, will have <a class="reference internal" href="#term-mrg"><span class="xref std std-term">.MRG</span></a> file containing the names of the
<em>MyISAM</em> tables associated with it.</dd>
<dt id="term-trg">.TRG</dt>
<dd>File containing the Triggers associated to a table, e.g.
<cite>:file:`mytable.TRG</cite>. With the <a class="reference internal" href="#term-trn"><span class="xref std std-term">.TRN</span></a> file, they represent all the
Trigger definitions.</dd>
<dt id="term-trn">.TRN</dt>
<dd>File containing the Triggers’ Names associated to a table, e.g.
<cite>:file:`mytable.TRN</cite>. With the <a class="reference internal" href="#term-trg"><span class="xref std std-term">.TRG</span></a> file, they represent all the
Trigger definitions.</dd>
<dt id="term-arm">.ARM</dt>
<dd>Each table with the <strong class="program">Archive Storage Engine</strong> has <code class="docutils literal"><span class="pre">.ARM</span></code> file
which contains the metadata of it.</dd>
<dt id="term-arz">.ARZ</dt>
<dd>Each table with the <strong class="program">Archive Storage Engine</strong> has <code class="docutils literal"><span class="pre">.ARZ</span></code> file
which contains the data of it.</dd>
<dt id="term-csm">.CSM</dt>
<dd>Each table with the <strong class="program">CSV Storage Engine</strong> has <code class="docutils literal"><span class="pre">.CSM</span></code> file which
contains the metadata of it.</dd>
<dt id="term-csv">.CSV</dt>
<dd>Each table with the <strong class="program">CSV Storage</strong> engine has <code class="docutils literal"><span class="pre">.CSV</span></code> file which
contains the data of it (which is a standard Comma Separated Value file).</dd>
<dt id="term-opt">.opt</dt>
<dd><em>MySQL</em> stores options of a database (like charset) in a file with a
<code class="file docutils literal"><span class="pre">.opt</span></code> extension in the database directory.</dd>
<dt id="term-par">.par</dt>
<dd>Each partitioned table has .par file which contains metadata about the
partitions.</dd>
</dl>