# LRU转储备份

Percona XtraBackup 将保存的缓冲池转储包括到备份中，以减少预热时间。重新启动后，它将从 ib_buffer_pool 文件恢复缓冲池状态。 Percona XtraBackup 发现 ib_buffer_pool 并自动对其进行备份。

![](https://www.percona.com/doc/percona-xtrabackup/LATEST/_images/lru_dump.png)

如果在 my.cnf 中启用了缓冲区还原选项，则在还原备份后，缓冲池将处于热状态。

参见

[MySQL Documentation: Saving and Restoring the Buffer Pool State](https://dev.mysql.com/doc/refman/8.0/en/innodb-preload-buffer-pool.html)
