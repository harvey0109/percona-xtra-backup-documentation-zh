# 将备份历史记录存储在服务器上

Percona XtraBackup 支持将备份历史记录存储在服务器上。此功能已在Percona XtraBackup 2.2 中实现。实现了将备份历史记录存储在服务器上的目的是为用户提供有关正在执行的备份的其他信息。备份历史记录信息将存储在 PERCONA_SCHEMA.XTRABACKUP_HISTORY 表中。

要使用此功能，可以使用以下选项：

- `--history = <name>`：此选项启用历史记录功能，并允许用户指定将放置在历史记录中的备份系列名称。
- `--incremental-history-name = <name>`：此选项允许基于名称的特定历史记录系列进行增量备份。 xtrabackup会在历史记录表中搜索该系列中最近（最高的to_lsn）备份，并使用to_lsn值作为从lsn开始的值。这与--incremental-history-uuid，-incremental-basedir和--incremental-lsn选项互斥。如果找不到有效的LSN（没有该名称的序列），xtrabackup将返回错误。
- `--incremental-history-uuid = <uuid>`：允许基于UUID标识的特定历史记录进行增量备份。 xtrabackup将搜索历史记录表以查找与UUID匹配的记录，并使用to_lsn值作为开始LSN时要使用的值。此选项与--incremental-basedir，--incremental-lsn和--incremental-history-name选项互斥。如果找不到有效的LSN（没有该UUID的记录或缺少to_lsn），则xtrabackup将返回错误。

::: tip Note
当前执行的备份将不会在生成的备份集中的xtrabackup_history表中存在，因为直到完成备份后，记录才会添加到该表中。
::: 

如果你想在一些灾难性事件中访问备份集以外的备份历史，你将需要在 xtrabackup 完成后对历史表进行mysqldump、部分备份或`SELECT *`，并将结果与你的备份集一起存储。

## 特权

执行备份的用户将需要以下特权：

- `CREATE`特权为了创建 PERCONA_SCHEMA.xtrabackup_history 数据库和表。
- `INSERT`特权，以便将历史记录添加到PERCONA_SCHEMA.xtrabackup_history表。
- `SELECT`特权，以便使用 --incremental-history-name 或 --incremental-history-uuid ，以使功能在 PERCONA_SCHEMA.xtrabackup_history 表中查找 innodb_to_lsn 值。

PERCONA_SCHEMA.XTRABACKUP_HISTORY 表

该表包含有关先前服务器备份的信息。仅当使用 --history 选项进行备份时，才会写入有关备份的信息。

| Column Name      | Description                                                  |
| :--------------- | :----------------------------------------------------------- |
| uuid             | 唯一的备份ID                                                 |
| name             | 用户提供的备份系列名称。可能有多个具有相同名称的条目，用于标识系列中的相关备份。 |
| tool_name        | 用于进行备份的工具的名称                                     |
| tool_command     | 为工具提供了精确的命令行，并混淆了–password和–encryption_key |
| tool_version     | 用于备份的工具版本                                           |
| ibbackup_version | 用于进行备份的xtrabackup二进制文件的版本                     |
| server_version   | 进行备份的服务器版本                                         |
| start_time       | 备份开始时间                                                 |
| end_time         | 备份结束时间                                                 |
| lock_time        | 为`FLUSH TABLES WITH READ LOCK`调用和保持锁的时间（秒）。    |
| binlog_pos       | Binlog文件和`FLUSH TABLES WITH READ LOCK`末尾的位置          |
| innodb_from_lsn  | 备份开始时的LSN，可用于确定以前的备份                        |
| innodb_to_lsn    | 备份结束时的LSN可用作下一个增量的起始lsn                     |
| partial          | 这是部分备份吗？如果N表示完全备份                            |
| incremental      | 这是增量备份吗                                               |
| format           | 结果格式说明（file，tar，xbstream）                          |
| compact          | 这是紧凑的备份吗                                             |
| compressed       | 这是压缩备份吗                                               |
| encrypted        | 这是加密的备份吗                                             |

局限性

- `--history` 选项必须仅在命令行上指定，而不能在配置文件中指定，这样才能生效。
- `--incremental-history-name` 和 `--incremental-history-uuid` 选项必须仅在命令行上指定，而不能在配置文件中指定，这样才能生效。

