# xbstream二进制

为了支持同时压缩和流媒体，除了TAR格式，Percona XtraBackup还引入了一种新的自定义流媒体格式，称为xbstream。这是为了克服传统归档格式的一些限制，如tar、cpio和其他不允许流媒体动态生成的文件，例如动态压缩的文件。与传统的流媒体/归档格式相比，xbstream的其他优点包括能够同时流式传输多个文件（所以有可能在xbstream格式中与-parallel选项一起使用流式传输）和更紧凑的数据存储。

该实用程序具有类似tar的界面：

- 使用-x选项，除非从-c选项中另外指定，否则它将从从其标准输入读取到当前目录的流中提取文件。 Percona XtraBackup 2.4.7已实现了对使用--parallel选项进行并行提取的支持。
- 用-c选项，它将命令行上指定的文件流到其标准输出。
- 如果指定了-decrypt=ALGO选项，xbstream将在提取输入流时自动解密加密的文件。这个选项的支持值是。AES128、AES192和AES256。必须指定--encrypt-key或--encrypt-key-file选项来提供加密密钥，但不能同时指定。这个选项已经在Percona XtraBackup 2.4.7中实现。
- 使用 --encrypt-reads 选项，你可以指定并行数据加密的线程数。默认值是1。这个选项已经在Percona XtraBackup 2.4.7中实现。
- --encrypt-key选项用于指定将使用的加密密钥。它不能与 --encrypt-key-file 选项一起使用，因为它们是相互排斥的。这个选项已经在Percona XtraBackup 2.4.7中实现。
- --encrypt-key-file选项用于指定包含加密密钥的文件。它不能和--encrypt-key选项一起使用，因为它们是相互排斥的。这个选项已经在Percona XtraBackup 2.4.7中实现。

该工具还试图通过使用适当的posix_fadvise()调用，尽量减少对操作系统页面缓存的影响。

当启用xtrabackup的压缩功能时，所有的数据都被压缩，包括交易日志文件和元数据文件，使用指定的压缩算法。目前唯一支持的算法是quicklz。

所产生的文件具有qpress归档格式，也就是说，每一个由xtrabackup产生的`*.qp`文件本质上是一个单文件的qpress归档，可以被qpress文件存档器提取和解压缩。这意味着不需要像tar.gz那样解压整个备份来恢复一个单表。

要解压单个文件，请用--解压选项运行xbstream。你可以通过传递-decompress-reads选项来控制用于解压的线程数量。

另外，文件可以使用qpress工具进行解压，该工具可以从这里下载。Qpress支持多线程解压。 