- # xbcrypt二进制文件

  为了支持备份的加密和解密，Percona XtraBackup中引入了新工具xbcrypt。

  该实用程序已按照xbstream二进制文件建模，可以在Percona XtraBackup外部执行加密和解密。 xbcrypt具有以下命令行选项：

  - `-d`, `--decrypt`

    解密输入到输出的数据。

  - `-i`, `--input=name`

    可选的输入文件。如果不指定，输入将从标准输入中读取。

  - `-o`, `--output=name`

    可选的输出文件。如果不指定，输出将被写到标准输出。

  - `-a`, `--encrypt-algo=name`

    加密算法。

  - `-k`, `--encrypt-key=name`

    加密密钥。

  - `-f`, `--encrypt-key-file=name`

    包含加密密钥的文件。

  - `-s`, `--encrypt-chunk-size=#`

    加密工作缓冲区的大小，单位是字节。默认值是64K。

  - `--encrypt-threads=#`

    该选项指定了用于并行加密/解密的工作线程的数量。

  - `-v`, `--verbose`

    显示详细状态输出。