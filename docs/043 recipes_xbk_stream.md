# 进行流式备份

流模式将备份以xbstream格式发送到STDOUT，而不是复制到第一个参数命名的目录中。你可以用管道将输出到本地文件，或者通过网络将输出到另一个服务器。

要提取产生的xbstream文件，你必须使用xbstream工具：`xbstream -x < backup.xbstream`。

使用xbstream的示例

示例1: 将备份流式传输到名为backup.xbstream的存档中

```
xtrabackup --backup --stream=xbstream --target-dir=./ > backup.xbstream
```

示例1: 将备份流式传输到名为backup.xbstream的压缩存档中

```
xtrabackup --backup --stream=xbstream --compress --target-dir=./ > backup.xbstream
```

加密备份

```
$ xtrabackup –backup –stream=xbstream ./ > backup.xbstream gzip - | openssl des3 -salt -k “password” > backup.xbstream.gz.des3
```

将备份解压缩到当前目录

```
xbstream -x <  backup.xbstream
```

将压缩后的备份直接发送到另一台主机并解压缩

```
xtrabackup --backup --compress --stream=xbstream --target-dir=./ | ssh user@otherhost "xbstream -x"
```

使用netcat将备份发送到另一台服务器。

```bash
# 在目标主机上：
$ nc -l 9999 | cat - > /data/backups/backup.xbstream
# 在源主机上：
$ xtrabackup --backup --stream=xbstream ./ | nc desthost 9999
```

使用单线将备份发送到另一台服务器：

```bash
$ ssh user@desthost “( nc -l 9999 > /data/backups/backup.xbstream & )” && xtrabackup –backup –stream=xbstream ./ | nc desthost 9999
```

使用管道查看器工具将吞吐量节制在10MB/秒[1]。

```
$ xtrabackup –backup –stream=xbstream ./ | pv -q -L10m ssh user@desthost “cat - > /data/backups/backup.xbstream”
```



在流传输期间对备份进行校验和：

```bash
# 在目标主机上：
nc -l 9999 | tee >(sha1sum > destination_checksum) > /data/backups/backup.xbstream
# 在源主机上：
xtrabackup --backup --stream=xbstream ./ | tee >(sha1sum > source_checksum) | nc desthost 9999
# 比较源主机上的校验和：
cat source_checksum
# 65e4f916a49c1f216e0887ce54cf59bf3934dbad  -
# 比较目标主机上的校验和：
cat destination_checksum
# 65e4f916a49c1f216e0887ce54cf59bf3934dbad  -
```

并行压缩与并行复制备份

```bash
xtrabackup --backup --compress --compress-threads=8 --stream=xbstream --parallel=4 --target-dir=./ > backup.xbstream
```

脚注

从官方网站或发行包中安装（`apt install pv`）。

