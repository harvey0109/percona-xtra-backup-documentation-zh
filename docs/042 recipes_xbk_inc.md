# 进行增量备份

备份所有InnoDB数据和日志文件--位于/var/lib/mysql/--一次，然后在/data/backups/mysql/（目的地）做两个每日增量备份。最后，准备好备份文件，以备恢复或使用。

## 创建一个完整备份

进行增量备份需要以完整备份为基础：

```
xtrabackup --backup --target-dir=/data/backups/mysql/
```

重要的是，您现在不要运行--prepare命令。

## 创建两个增量备份

假设完整备份在星期一，而您将在星期二创建一个增量备份：

```
xtrabackup --backup --target-dir=/data/backups/inc/tue/ \
      --incremental-basedir=/data/backups/mysql/
```

并且在周三采用了相同的政策：

```
xtrabackup --backup --target-dir=/data/backups/inc/wed/ \
       --incremental-basedir=/data/backups/inc/tue/
```

## 准备基础备份

准备基本备份（星期一的备份）：

```
xtrabackup --prepare --apply-log-only --target-dir=/data/backups/mysql/
```

## 将基础数据前滚到第一个增量

将星期一的数据转发到星期二的州：

```
xtrabackup --prepare --apply-log-only --target-dir=/data/backups/mysql/ \
   --incremental-dir=/data/backups/inc/tue/
```

## 再次向前滚动到第二个增量

在周三再次向前滚动到该州：

```
xtrabackup --prepare --apply-log-only --target-dir=/data/backups/mysql/ \
   --incremental-dir=/data/backups/inc/wed/
```

## 准备整个备份以备使用

通过准备新日志来创建它：

```
xtrabackup --prepare --target-dir=/data/backups/mysql/
```

## 笔记

- 如果你在一个有足够可用内存的专用服务器上，你可能想设置 [`--use-memory`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-use-memory) 来加快进程。更多细节请看[here](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html).。
- 更详细的解释在[here](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/incremental_backups.html).。

