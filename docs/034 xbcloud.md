# xbcloud二进制文件

xbcloud的目的是为了从云端下载和上传全部或部分xbstream归档文件。xbcloud不会覆盖同名的备份。xbcloud通过管道接受来自xbstream的输入，因此它可以作为一个管道与xtrabackup一起调用，直接流向云端而不需要本地存储。

xbcloud将每个块存储为一个单独的对象，名称为backup_name/database/table.ibd.NNNNNNNNNNN，其中NNN...是文件中块的0填充序号。由xtrabackup和xbstream产生的块的大小改为10M。

xbcloud有三个基本操作：put、get和delete。通过这些操作，可以创建、存储、检索、恢复和删除备份。xbcloud的操作显然与AWS S3 API中的类似操作相吻合。

## 支持的云存储类型

除了Swift（在Percona XtraBackup 2.4.14之前，Swift一直是在云存储中存储备份的唯一选择），xbcloud支持Amazon S3、MinIO和谷歌云存储。其他与Amazon S3兼容的存储空间，如Wasabi或Digital Ocean Spaces，也被支持。

See also

- OpenStack Object Storage (“Swift”)

  https://wiki.openstack.org/wiki/Swift

- Amazon Simple Storage Service

  https://aws.amazon.com/s3/

- MinIO

  https://min.io/

- Google Cloud Storage

  https://cloud.google.com/storage/

- Wasabi

  https://wasabi.com/

- Digital Ocean Spaces

  https://www.digitalocean.com/products/spaces/

## 用法

```
$ xtrabackup --backup --stream=xbstream --target-dir=/tmp | xbcloud \
put [options] <name>
```

## 使用Swift创建完整备份

下面的例子显示了如何做一个完整的备份，并将其上传到Swift。

```
$ xtrabackup --backup --stream=xbstream --extra-lsndir=/tmp --target-dir=/tmp | \
xbcloud put --storage=swift \
--swift-container=test \
--swift-user=test:tester \
--swift-auth-url=http://192.168.8.80:8080/ \
--swift-key=testing \
--parallel=10 \
full_backup
```

## 使用Amazon S3创建完整备份

```
$ xtrabackup --backup --stream=xbstream --extra-lsndir=/tmp --target-dir=/tmp | \
xbcloud put --storage=s3 \
--s3-endpoint='s3.amazonaws.com' \
--s3-access-key='YOUR-ACCESSKEYID' \
--s3-secret-key='YOUR-SECRETACCESSKEY' \
--s3-bucket='mysql_backups'
--parallel=10 \
$(date -I)-full_backup
```

使用Amazon S3时，以下选项可用：

| Option                                | Details                                                      |
| :------------------------------------ | :----------------------------------------------------------- |
| –s3-access-key                        | 用于提供AWS访问密钥ID                                        |
| –s3-secret-key                        | 用于提供AWS秘密访问密钥                                      |
| –s3-bucket                            | 使用提供的AWS存储桶名称                                      |
| –s3-region                            | 用于指定AWS区域。默认值为us-east-1                           |
| –s3-api-version = <AUTO\|2\|4>        | 选择签名算法。默认值为AUTO。在这种情况下，xbcloud将进行探测。 |
| –s3-bucket-lookup = <AUTO\|PATH\|DNS> | 指定是使用bucket.endpoint.com还是endpoint.com/bucket*样式请求。默认值为AUTO。在这种情况下，xbcloud将进行探测。 |
| –s3-storage-class=<name>              | 指定S3存储类别。默认存储类取决于提供程序。名称选项如下：STANDARD STANDARD_IA GLACIER, 如果你使用GLACIER存储类，在恢复备份之前，必须将对象恢复到S3。 也支持使用自定义的S3实现，如MinIO或CephRadosGW。 |

使用MinIO创建完整备份

```
$ xtrabackup --backup --stream=xbstream --extra-lsndir=/tmp --target-dir=/tmp | \
xbcloud put --storage=s3 \
--s3-endpoint='play.minio.io:9000' \
--s3-access-key='YOUR-ACCESSKEYID' \
--s3-secret-key='YOUR-SECRETACCESSKEY' \
--s3-bucket='mysql_backups'
--parallel=10 \
$(date -I)-full_backup
```

## 使用Google云端存储创建完整备份

对谷歌云存储的支持是使用互操作性模式实现的。这种模式是专门为与亚马逊S3兼容的云服务互动而设计的。

See also

- Cloud Storage Interoperability

  https://cloud.google.com/storage/docs/interoperability

```
$ xtrabackup --backup --stream=xbstream --extra-lsndir=/tmp --target-dir=/tmp | \
xbcloud put --storage=google \
--google-endpoint=`storage.googleapis.com` \
--google-access-key='YOUR-ACCESSKEYID' \
--google-secret-key='YOUR-SECRETACCESSKEY' \
--google-bucket='mysql_backups'
--parallel=10 \
$(date -I)-full_backup
```

使用谷歌云存储时有以下选项。

- –google-access-key = <ACCESS KEY ID>
- –google-secret-key = <SECRET ACCESS KEY>
- –google-bucket = <BUCKET NAME>
- –google-storage-class=name

::: tip Note

Google存储类别名称选项如下：

- STANDARD
- NEARLINE
- COLDLINE
- ARCHIVE

:::

See also

[Google storage classes](https://cloud.google.com/storage/docs/storage-classes) [The default Google storage class depends on the storage class of the bucket](https://cloud.google.com/storage/docs/changing-default-storage-class)

## 提供参数

每种存储类型都有必填参数，您可以在命令行，配置文件中以及通过环境变量提供这些参数。

### 配置文件

值不经常变化的参数可以存储在my.cnf或自定义配置文件中。下面的例子是[xbcloud]组下的配置选项的一个模板。

```
[xbcloud]
storage=s3
s3-endpoint=http://localhost:9000/
s3-access-key=minio
s3-secret-key=minio123
s3-bucket=backupsx
s3-bucket-lookup=path
s3-api-version=4
```

::: tip Note

如果您在命令行和配置文件中明确使用一个参数，xbcloud会使用命令行上提供的值。

:::

### 环境变量

xbcloud会识别以下环境变量，并自动将它们映射到适用于选定存储的相应参数。

- AWS_ACCESS_KEY_ID (or ACCESS_KEY_ID)
- AWS_SECRET_ACCESS_KEY (or SECRET_ACCESS_KEY)
- AWS_DEFAULT_REGION (or DEFAULT_REGION)
- AWS_ENDPOINT (or ENDPOINT)
- AWS_CA_BUNDLE

::: tip Note

如果您在命令行或配置文件中明确地使用一个参数，并且相应的环境变量包含一个值，那么xbcloud会使用在命令行或配置文件中提供的那个值。

:::

OpenStack环境变量也会被识别并自动映射到相应的swift参数（--storage=swift）。

- OS_AUTH_URL
- OS_TENANT_NAME
- OS_TENANT_ID
- OS_USERNAME
- OS_PASSWORD
- OS_USER_DOMAIN
- OS_USER_DOMAIN_ID
- OS_PROJECT_DOMAIN
- OS_PROJECT_DOMAIN_ID
- OS_REGION_NAME
- OS_STORAGE_URL
- OS_CACERT

### 捷径

对于所有的操作（放、取和删除），你可以使用一个快捷方式来指定存储类型、桶名和备份名作为一个参数，而不是使用三个不同的参数（-storage、-s3-桶和备份名本身）。

使用快捷的语法来提供存储类型、桶和备份名称

使用以下格式：storage-type://bucket-name/backup-name

```
$ xbcloud get s3://operator-testing/bak22 ...
```

在这个例子中，s3指的是一个存储类型，operator-testing是一个桶的名称，bak22是备份名称。这个快捷方式的展开方式如下。

```
$ xbcloud get --storage=s3 --s3-bucket=operator-testing bak22 ...
```

你不仅可以在命令行上提供强制性参数。你可以使用配置文件和环境变量。

### 附加参数

xbcloud接受额外的参数，你可以对任何存储类型使用。--md5参数可以计算出备份块的MD5哈希值。计算结果将存储在遵循backup_name.md5模式的文件中。

```
$ xtrabackup --backup --stream=xbstream \
--parallel=8 2>backup.log | xbcloud put s3://operator-testing/bak22 \
--parallel=8 --md5 2>upload.log
```

你可以使用`--header`参数来传递一个额外的HTTP头与服务器端的加密，同时指定一个客户密钥。

使用–header进行AES256加密的示例

```
$ xtrabackup --backup --stream=xbstream --parallel=4 | \
xbcloud put s3://operator-testing/bak-enc/ \
--header="X-Amz-Server-Side-Encryption-Customer-Algorithm: AES256" \
--header="X-Amz-Server-Side-Encryption-Customer-Key: CuStoMerKey=" \
--header="X-Amz-Server-Side-Encryption-Customer-Key-MD5: CuStoMerKeyMd5==" \
--parallel=8
```

--header参数对于设置访问控制列表（ACL）的权限也很有用。--header="x-amz-acl: bucket-owner-full-control

## 使用Swift恢复

```
$ xbcloud get [options] <name> [<list-of-files>] | xbstream -x
```

下面的例子显示了如何从Swift获取和恢复备份。

```
$ xbcloud get --storage=swift \
--swift-container=test \
--swift-user=test:tester \
--swift-auth-url=http://192.168.8.80:8080/ \
--swift-key=testing \
full_backup | xbstream -xv -C /tmp/downloaded_full

$ xbcloud delete --storage=swift --swift-user=xtrabackup \
--swift-password=xtrabackup123! --swift-auth-version=3 \
--swift-auth-url=http://openstack.ci.percona.com:5000/ \
--swift-container=mybackup1 --swift-domain=Default
```

## 使用Amazon S3还原

```
$ xbcloud get s3://operator-testing/bak22 \
--s3-endpoint=https://storage.googleapis.com/ \
--parallel=10 2>download.log | xbstream -x -C restore --parallel=8
```

## 增量备份

首先，你需要做一个完整的备份，作为增量备份的基础。

```
xtrabackup --backup --stream=xbstream --extra-lsndir=/storage/backups/ \
--target-dir=/storage/backups/ | xbcloud put \
--storage=swift --swift-container=test_backup \
--swift-auth-version=2.0 --swift-user=admin \
--swift-tenant=admin --swift-password=xoxoxoxo \
--swift-auth-url=http://127.0.0.1:35357/ --parallel=10 \
full_backup
```

然后，您可以进行增量备份：

```
$ xtrabackup --backup --incremental-basedir=/storage/backups \
--stream=xbstream --target-dir=/storage/inc_backup | xbcloud put \
--storage=swift --swift-container=test_backup \
--swift-auth-version=2.0 --swift-user=admin \
--swift-tenant=admin --swift-password=xoxoxoxo \
--swift-auth-url=http://127.0.0.1:35357/ --parallel=10 \
inc_backup
```

### 准备增量备份

要准备备份，您首先需要下载完整备份：

```
$ xbcloud get --swift-container=test_backup \
--swift-auth-version=2.0 --swift-user=admin \
--swift-tenant=admin --swift-password=xoxoxoxo \
--swift-auth-url=http://127.0.0.1:35357/ --parallel=10 \
full_backup | xbstream -xv -C /storage/downloaded_full
```

下载完整备份后，应做好以下准备：

```
$ xtrabackup --prepare --apply-log-only --target-dir=/storage/downloaded_full
```

完整备份准备好后，你可以下载增量备份。

```
$ xbcloud get --swift-container=test_backup \
--swift-auth-version=2.0 --swift-user=admin \
--swift-tenant=admin --swift-password=xoxoxoxo \
--swift-auth-url=http://127.0.0.1:35357/ --parallel=10 \
inc_backup | xbstream -xv -C /storage/downloaded_inc
```

一旦下载了增量备份，你可以通过运行来准备它。

```
$ xtrabackup --prepare --apply-log-only \
--target-dir=/storage/downloaded_full \
--incremental-dir=/storage/downloaded_inc

$ xtrabackup --prepare --target-dir=/storage/downloaded_full
```

### 部分下载云备份

如果你不想下载整个备份来恢复特定的数据库，你可以只指定你要恢复的表。

```
$ xbcloud get --swift-container=test_backup
--swift-auth-version=2.0 --swift-user=admin \
--swift-tenant=admin --swift-password=xoxoxoxo \
--swift-auth-url=http://127.0.0.1:35357/ full_backup \
ibdata1 sakila/payment.ibd \
> /storage/partial/partial.xbs

$ xbstream -xv -C /storage/partial < /storage/partial/partial.xbs
```

这个命令将从完整备份中只下载ibdata1和sakila/payment.ibd表。

## 命令行选项

xbcloud具有以下命令行选项：

`--storage=[swift|s3|google]`
云存储选项。xbcloud支持Swift、MinIO和AWS S3。默认值是swift。

`--swift-auth-url`
Swift集群的URL。

`--swift-storage-url`
xbcloud将尝试从keystone响应中获取指定区域（如果有指定的话）的对象存储URL。人们可以通过传递-swift-存储-url=URL参数来覆盖该URL。

`--swift-user`
Swift用户名（X-Auth-User，特定于Swift）。

`--swift-key`
Swift钥匙/密码（X-Auth-Key，特定于Swift）。

`--swift-container`
要备份到的容器（特定于Swift）

`--parallel=N`
并发上传/下载线程的最大数量。默认为1。

`--cacert`
具有CA证书的文件的路径

`--insecure`
不验证服务器证书

### Swift身份验证选项

Swift规范描述了几个认证选项。xbcloud可以通过API版本2和3对keystone进行认证。

- `--swift-auth-version`

  指定swift认证版本。可能的值是。1.0 - TempAuth，2.0 - Keystone v2.0，和 3 - Keystone v3。默认值是1.0。

对于v2，其他选项包括：

- `--swift-tenant`

  Swift租户名称。

- `--swift-tenant-id`

  Swift租户ID。

- `--swift-region`

  Swift端点区域。

- `--swift-password`

  用户的Swift密码。

对于v3，其他选项是：

- `--swift-user-id`

  Swift用户ID。

- `--swift-project`

  Swift项目名称。

- `--swift-project-id`

  Swift项目ID。

- `--swift-domain`

  Swift域名。

- `--swift-domain-id`

  Swift域ID。

