# 加密备份

Percona XtraBackup 支持通过 xbstream 选项加密和解密本地备份和流式备份，从而增加了另一层保护。加密是使用 GnuPG 的 libgcrypt 库实现的。

## 创建加密的备份

要进行加密备份，需要指定以下选项（选项 --encrypt-key 和 --encrypt-key-file 是互斥的，即仅需要提供其中之一）：

- `--encrypt`
- option: ` –encrypt-key`
- option: ` –encrypt-key-file`

--encrypt-key 选项和 --encrypt-key-file 选项均可用于指定加密密钥。可以使用 `openssl rand -base64 24` 之类的命令生成加密密钥。

该命令的示例输出应如下所示：

```
GCHFLrDFVx6UAsRb88uLVbAVWbK+Yzfs
```

然后可以将此值用作加密密钥

###  --encrypt-key

使用 --encrypt-key 的xtrabackup 命令示例应如下所示：

```
$  xtrabackup --backup --encrypt=AES256 --encrypt-key="GCHFLrDFVx6UAsRb88uLVbAVWbK+Yzfs" --target-dir=/data/backup
```

### --encrypt-key-file

使用 --encrypt-key-file 选项，如下所示：

```
$ xtrabackup --backup --encrypt=AES256 --encrypt-key-file=/data/backups/keyfile --target-dir=/data/backup
```

::: tip Note
根据用于制作 KEYFILE 的文本编辑器，编辑器可以自动插入CRLF（行尾）字符。这将导致密钥大小增加，从而使其无效。建议的创建文件的方法是使用命令行：`echo -n "GCHFLrDFVx6UAsRb88uLVbAVWbK+Yzfs"> /data/backups/keyfile`。
:::


## 优化加密过程

加密备份有两个新选项，可用于加快加密过程。它们是 --encrypt-threads 和 --encrypt-chunk-size 。通过使用 --encrypt-threads 选项，可以指定多个线程用于并行加密。选项  --encrypt-chunk-size 可用于指定每个加密线程的工作加密缓冲区的大小（以字节为单位）（默认为64K）。

## 解密加密的备份

可以使用 xbcrypt 二进制文件解密备份。以下单线可用于加密整个文件夹：

```
$ for i in `find . -iname "*\.xbcrypt"`; do xbcrypt -d --encrypt-key-file=/root/secret_key --encrypt-algo=AES256 < $i > $(dirname $i)/$(basename $i .xbcrypt) && rm $i; done
```

Percona XtraBackup --decrypt 选项已实现，可用于解密备份：

```
$ xtrabackup --decrypt=AES256 --encrypt-key="GCHFLrDFVx6UAsRb88uLVbAVWbK+Yzfs" --target-dir=/data/backup/
```


Percona XtraBackup 不会自动删除加密的文件。为了清理备份目录，用户应删除 `*.xbcrypt` 文件。

::: tip Note
--parallel 可与 --decrypt 选项一起使用，以同时解密多个文件。
:::

解密文件后，即可准备备份。

## 准备加密的备份

备份解密后，可以使用 --prepare 选项以与标准完全备份相同的方式进行准备：

```
$ xtrabackup --prepare --target-dir=/data/backup/
```

## 恢复加密的备份

xtrabackup提供了 --copy-back 选项，可将备份恢复到服务器的数据目录：

```
$ xtrabackup --copy-back --target-dir=/data/backup/
```

它将所有与数据相关的文件复制回服务器的datadir，该文件由服务器的 my.cnf 配置文件确定。您应该检查输出的最后一行以获取成功消息：

```
150318 11:08:13  xtrabackup: completed OK!
```

参考

GnuPG文档：`libgcrypt` 库 [http://www.gnupg.org/documentation/manuals/gcrypt/](http://www.gnupg.org/documentation/manuals/gcrypt/)










