# 分析表统计信息

xtrabackup 二进制文件可以以只读模式分析 InnoDB 数据文件，以提供有关它们的统计信息。为此，您应使用 --stats 选项。您可以将其与 --tables 选项结合使用以限制要检查的文件。它还使用 --use-memory 选项。

您可以在运行中的服务器上执行分析，由于在分析过程中更改了数据，因此可能会出现错误。或者，您可以分析数据库的备份副本。无论哪种方式，要使用统计信息功能，都需要数据库的干净副本，包括大小正确的日志文件，因此，您需要使用 --prepare 执行两次，才能在备份上使用此功能。

在备份上运行的结果可能如下所示：

```
<INDEX STATISTICS>
  table: test/table1, index: PRIMARY, space id: 12, root page 3
  estimated statistics in dictionary:
    key vals: 25265338, leaf pages 497839, size pages 498304
  real statistics:
     level 2 pages: pages=1, data=5395 bytes, data/pages=32%
     level 1 pages: pages=415, data=6471907 bytes, data/pages=95%
        leaf pages: recs=25958413, pages=497839, data=7492026403 bytes, data/pages=91%
```

可以解释如下：

- 第一行仅显示表和索引名称及其内部标识符。如果您看到一个名为 GEN_CLUST_INDEX 的索引，它是表的聚集索引，因为您没有显式创建PRIMARY KEY，所以它是自动创建的。
字典信息中的估计统计信息类似于通过InnoDB内部的ANALYZE TABLE收集的数据，这些数据将存储为估计基数统计信息并传递给查询优化器。
- 实际的统计信息是扫描数据页并计算有关索引的准确信息的结果。
- `The level <X> pages`：输出表示该行在索引树中显示有关该级别页面的信息。 \<X\>越大，它距级别为0的叶子页越远。第一行是根页。
- `leaf pages` 输出显示叶子页面。这是表格数据的存储位置。
- `external pages`：输出（未显示）显示了较大的外部页面，这些页面的值太长而无法放入行本身，例如长BLOB和TEXT值。
- `recs`是叶子页中的实际记录（行）数。
- `page` 是页数。
- `data` 是页面中数据的总大小，以字节为单位。
- `data/pages` 的计算方式为（data/（pages* PAGE_SIZE））* 100％。由于为页眉和页脚保留的空间，它永远不会达到100％。

一个更详细的示例作为 MySQL Performance Blog [帖子](http://www.mysqlperformanceblog.com/2009/09/14/statistics-of-innodb-tables-and-indexes-available-in-xtrabackup/)发布。

## 格式化输出的脚本

以下脚本可用于汇总和列表统计信息的输出：

```
tabulate-xtrabackup-stats.pl

#!/usr/bin/env perl
use strict;
use warnings FATAL => 'all';
my $script_version = "0.1";

my $PG_SIZE = 16_384; # InnoDB defaults to 16k pages, change if needed.
my ($cur_idx, $cur_tbl);
my (%idx_stats, %tbl_stats);
my ($max_tbl_len, $max_idx_len) = (0, 0);
while ( my $line = <> ) {
   if ( my ($t, $i) = $line =~ m/table: (.*), index: (.*), space id:/ ) {
      $t =~ s!/!.!;
      $cur_tbl = $t;
      $cur_idx = $i;
      if ( length($i) > $max_idx_len ) {
         $max_idx_len = length($i);
      }
      if ( length($t) > $max_tbl_len ) {
         $max_tbl_len = length($t);
      }
   }
   elsif ( my ($kv, $lp, $sp) = $line =~ m/key vals: (\d+), \D*(\d+), \D*(\d+)/ ) {
      @{$idx_stats{$cur_tbl}->{$cur_idx}}{qw(est_kv est_lp est_sp)} = ($kv, $lp, $sp);
      $tbl_stats{$cur_tbl}->{est_kv} += $kv;
      $tbl_stats{$cur_tbl}->{est_lp} += $lp;
      $tbl_stats{$cur_tbl}->{est_sp} += $sp;
   }
   elsif ( my ($l, $pages, $bytes) = $line =~ m/(?:level (\d+)|leaf) pages:.*pages=(\d+), data=(\d+) bytes/ ) {
      $l ||= 0;
      $idx_stats{$cur_tbl}->{$cur_idx}->{real_pages} += $pages;
      $idx_stats{$cur_tbl}->{$cur_idx}->{real_bytes} += $bytes;
      $tbl_stats{$cur_tbl}->{real_pages} += $pages;
      $tbl_stats{$cur_tbl}->{real_bytes} += $bytes;
   }
}

my $hdr_fmt = "%${max_tbl_len}s %${max_idx_len}s %9s %10s %10s\n";
my @headers = qw(TABLE INDEX TOT_PAGES FREE_PAGES PCT_FULL);
printf $hdr_fmt, @headers;

my $row_fmt = "%${max_tbl_len}s %${max_idx_len}s %9d %10d %9.1f%%\n";
foreach my $t ( sort keys %tbl_stats ) {
   my $tbl = $tbl_stats{$t};
   printf $row_fmt, $t, "", $tbl->{est_sp}, $tbl->{est_sp} - $tbl->{real_pages},
      $tbl->{real_bytes} / ($tbl->{real_pages} * $PG_SIZE) * 100;
   foreach my $i ( sort keys %{$idx_stats{$t}} ) {
      my $idx = $idx_stats{$t}->{$i};
      printf $row_fmt, $t, $i, $idx->{est_sp}, $idx->{est_sp} - $idx->{real_pages},
         $idx->{real_bytes} / ($idx->{real_pages} * $PG_SIZE) * 100;
   }
}
```

### 示例脚本输出

当与前面提到的博客文章中显示的示例运行时，上述Perl脚本的输出将如下所示：

```
TABLE           INDEX TOT_PAGES FREE_PAGES   PCT_FULL
art.link_out104                    832383      38561      86.8%
art.link_out104         PRIMARY    498304         49      91.9%
art.link_out104       domain_id     49600       6230      76.9%
art.link_out104     domain_id_2     26495       3339      89.1%
art.link_out104 from_message_id     28160        142      96.3%
art.link_out104    from_site_id     38848       4874      79.4%
art.link_out104   revert_domain    153984      19276      71.4%
art.link_out104    site_message     36992       4651      83.4%
```

这些列是表和索引，然后是该索引中的页面总数，数据未实际占用的页面数以及实际数据的字节数（占实际数据页面总大小的百分比） 。上面输出中的第一行（INDEX列为空）是整个表的摘要。
