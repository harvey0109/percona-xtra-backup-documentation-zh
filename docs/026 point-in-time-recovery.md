# 时间点恢复

可以使用 xtrabackup 和服务器的二进制日志来恢复数据库历史记录中的特定时刻。

请注意，二进制日志包含从过去的某个点修改数据库的操作。您需要一个完整的datadir作为基础，然后可以从二进制日志中应用一系列操作，以使数据与所需时间点的数据匹配。

```
$ xtrabackup --backup --target-dir=/path/to/backup
$ xtrabackup --prepare --target-dir=/path/to/backup
```

有关这些过程的更多详细信息，请参阅创建备份和准备备份。

现在，假设已经过去了一段时间，并且您想将数据库还原到过去的某个点，请记住要限制快照的拍摄点。

要了解服务器中二进制日志记录的情况，请执行以下查询：

```sql
mysql> SHOW BINARY LOGS;
+------------------+-----------+
| Log_name         | File_size |
+------------------+-----------+
| mysql-bin.000001 |       126 |
| mysql-bin.000002 |      1306 |
| mysql-bin.000003 |       126 |
| mysql-bin.000004 |       497 |
+------------------+-----------+
```

和

```sql
mysql> SHOW MASTER STATUS;
+------------------+----------+--------------+------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+------------------+----------+--------------+------------------+
| mysql-bin.000004 |      497 |              |                  |
+------------------+----------+--------------+------------------+
```

第一个查询将告诉您哪些文件包含二进制日志，第二个查询将告诉您当前正在使用哪个文件记录更改，以及其中的当前位置。这些文件通常存储在datadir中（除非使用`--log-bin =`选项启动服务器时指定了其他位置）。

要了解拍摄快照的位置，请参阅备份目录中的 xtrabackup_binlog_info ：

```
$ cat /path/to/backup/xtrabackup_binlog_info
mysql-bin.000003      57
```

这将告诉您在备份时使用了哪个文件作为二进制日志及其位置。当您还原备份时，该位置将是有效的位置：

```
$ xtrabackup --copy-back --target-dir=/path/to/backup
```

由于还原不会影响二进制日志文件（您可能需要调整文件许可权，请参阅还原备份），因此下一步是从快照的位置开始，使用 mysqlbinlog 从二进制日志中提取查询，并将其重定向到文件

```
$ mysqlbinlog /path/to/datadir/mysql-bin.000003 /path/to/datadir/mysql-bin.000004 \
    --start-position=57 > mybinlog.sql
```

请注意，如示例中所示，如果二进制日志有多个文件，则必须使用一个进程来提取查询，如上所示。

使用查询检查文件，以确定哪个位置或日期与所需的时间点相对应。确定后，将其通过管道传输到服务器。假设点是11-12-25 01:00:00：

```
$ mysqlbinlog /path/to/datadir/mysql-bin.000003 /path/to/datadir/mysql-bin.000004 \
    --start-position=57 --stop-datetime="11-12-25 01:00:00" | mysql -u root -p
```

并且数据库将前滚到该时间点。

