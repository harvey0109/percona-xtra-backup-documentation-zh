# 用户的特权和权限

我们所说的权限是指用户访问和执行主机文件系统相关部分的更改、启动/停止服务和安装软件的能力。

我们所说的权限是指数据库用户在数据库服务器上执行不同类型操作的能力。

## 在系统级别

有许多方法可以检查一个文件或目录的权限。例如，`ls -ls /path/to/file`或`stat /path/to/file | grep Access`将完成这项工作。

```
$ stat /etc/mysql | grep Access
Access: (0755/drwxr-xr-x)  Uid: (    0/    root)   Gid: (    0/    root)
Access: 2011-05-12 21:19:07.129850437 -0300
$ ls -ld /etc/mysql/my.cnf
-rw-r--r-- 1 root root 4703 Apr  5 06:26 /etc/mysql/my.cnf
```

在这个例子中，my.cnf是由root拥有的，其他任何人都不能写。假设你没有root的密码，你可以用sudo -l检查你在这类文件上有什么权限。

```
$ sudo -l
Password:
You may run the following commands on this host:
(root) /usr/bin/
(root) NOPASSWD: /etc/init.d/mysqld
(root) NOPASSWD: /bin/vi /etc/mysql/my.cnf
(root) NOPASSWD: /usr/local/bin/top
(root) NOPASSWD: /usr/bin/ls
(root) /bin/tail
```

能够用/etc/init.d/、/etc/rc.d/或/sbin/service中的sudo脚本执行是启动和停止服务的能力。

另外，如果你能执行你的发行版的软件包管理器，你可以用它来安装或删除软件。如果不能，在一个目录上拥有rwx权限可以让你通过在那里编译软件来进行本地安装。这是许多托管公司服务中的一个典型情况。

还有其他管理权限的方法，例如使用 PolicyKit、Extended ACLs 或 SELinux，它们可能会阻止或允许你的访问。在这种情况下，你应该检查它们。

## 在数据库服务器级别

要查询已授予数据库用户的特权，请在服务器的控制台上执行：

```
mysql> SHOW GRANTS;
```

或针对具有以下特征的特定用户：

```
mysql> SHOW GRANTS FOR 'db-user'@'host';
```

它将使用与GRANT语句相同的格式显示权限。

请注意，不同版本的服务器的权限可能有所不同。要列出你的服务器支持的确切权限列表（以及对它们的简要描述），请执行。

```
mysql> SHOW PRIVILEGES;
```







