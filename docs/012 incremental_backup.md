# 增量备份

<!-- 已检查 -->

xtrabackup 支持增量备份, 这意味着它们只能复制自上次备份以来已更改的数据. 

您可以在每个完整备份之间执行许多增量备份, 因此可以设置备份过程, 例如每周一次完整备份和每天增量备份, 或者每天完整备份和每小时增量备份. 

增量备份之所以有效, 是因为每个 InnoDB page 都包含一个日志序列号或 LSN .  LSN 是整个数据库的系统版本号. 每个页面的LSN都显示了最近的更改时间. 

增量备份将复制其 LSN 比先前的增量或完整备份的 LSN 更新的每个页面. 有两种算法用于查找要复制的此类页面集. 第一个可用于所有服务器类型和版本的服务器是通过读取所有数据页直接检查 LSN 页.  Percona Server for MySQL 中提供的第二个功能是启用服务器上已更改的页面跟踪功能, 该功能将在更改页面时记录下来. 然后将这些信息写在一个紧凑的单独的所谓的位图文件中.  xtrabackup 二进制文件将使用该文件来读取增量备份所需的数据页, 从而可能保存许多读取请求. 如果 xtrabackup 二进制文件找到了位图文件, 则默认情况下启用后一种算法. 即使位图数据可用, 也可以指定 `--incremental-force-scan` 读取所有页面. 

::: tip Important

增量备份实际上不会将数据文件与先前备份的数据文件进行比较. 因此, 在部分备份之后运行增量备份可能会导致数据不一致. 

增量备份只需阅读页面, 然后将其 LSN 与上次备份的 LSN 进行比较即可. 但是, 您仍然需要完整备份来恢复增量更改. 没有完整的备份作为基础, 增量备份将毫无用处. 

如果您知道它的 LSN, 则可以使用 `--incremental-lsn` 选项执行增量备份, 而无需进行以前的备份. 
::: 

参见 [部分备份](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/partial_backups.html#pxb-partial-backup)

## 创建增量备份

要进行增量备份, 请像往常一样从完整备份开始.  xtrabackup 二进制文件将名为 xtrabackup_checkpoints 的文件写入备份的目标目录. 该文件包含一行显示 `to_lsn` 的行, 该行是备份结束时数据库的 LSN . 使用以下命令创建完整备份：


```bash
$ xtrabackup --backup --target-dir=/data/backups/base
```

如果查看 xtrabackup_checkpoints 文件, 则应根据您的 LSN nuber 看到类似的内容：

```
backup_type = full-backuped
from_lsn = 0
to_lsn = 1626007
last_lsn = 1626007
compact = 0
recover_binlog_info = 1
```

现在您已拥有完整备份, 您可以基于它进行增量备份. 使用以下命令：

```
$ xtrabackup --backup --target-dir=/data/backups/inc1 \
--incremental-basedir=/data/backups/base
```

`/data/backups/inc1/` 目录现在应该包含增量文件, 例如 `ibdata1.delta` 和 `test/table1.ibd.delta` . 这些代表自 `LSN 1626007` 以来的更改. 如果检查此目录中的 `xtrabackup_checkpoints` 文件, 则应该看到与以下内容相似的内容：

```
backup_type = incremental
from_lsn = 1626007
to_lsn = 4124244
last_lsn = 4124244
compact = 0
recover_binlog_info = 1
```

`from_lsn` 是备份的起始 LSN , 对于增量备份, 它必须与 先前/基本备份 的 to_lsn（如果它是最后一个检查点）相同. 

现在可以将此目录用作另一个增量备份的基础：

```
xtrabackup --backup --target-dir=/data/backups/inc2 \
--incremental-basedir=/data/backups/inc1
```

此文件夹还包含 xtrabackup_checkpoints：

```
backup_type = incremental
from_lsn = 4124244
to_lsn = 6938371
last_lsn = 7110572
compact = 0
recover_binlog_info = 1
```

::: tip Note

在这种情况下, 您可以看到to_lsn（最后一个检查点LSN）和 last_lsn（最后一个复制的LSN）之间存在差异, 这意味着在备份过程中服务器上有一些流量. 
::: 

## 准备增量备份

增量备份的 `--prepare` 步骤与完全备份不同. 在完全备份中, 执行两种类型的操作以使数据库保持一致：已提交的事务相对于数据文件从日志文件中重放, 未提交的事务被回滚. 在准备增量备份时, 您必须跳过未提交事务的回滚, 因为在备份时未提交的事务可能正在进行中, 并且很可能会在下一次增量备份中提交. 您应该使用 `--apply-log-only` 选项来防止回滚阶段. 

::: tip Warning

如果不使用 --apply-log-only 选项来防止回滚阶段, 则增量备份将无用. 事务回滚后, 无法再应用增量备份. 
::: 

从您创建的完整备份开始, 您可以准备它, 然后将增量差异应用于它. 回想一下, 您有以下备份：

```
/data/backups/base
/data/backups/inc1
/data/backups/inc2
```

要准备基本备份, 您需要像往常一样运行 `--prepare` , 但要防止回滚阶段：

```
$ xtrabackup --prepare --apply-log-only --target-dir=/data/backups/base
```

输出应以类似于以下内容的文本结尾：

```
InnoDB: Shutdown completed; log sequence number 1626007
161011 12:41:04 completed OK!
```

日志序列号应与您先前看到的基本备份的 to_lsn 相匹配. 

::: tip Note

即使已跳过回滚阶段, 此备份实际上现在也可以安全地按原样还原. 如果还原它并启动 MySQL ,  InnoDB 将检测到未执行回滚阶段, 它将在后台执行该操作, 就像启动时进行崩溃恢复一样. 它将通知您数据库未正常关闭. 
::: 


要将第一个增量备份应用于完整备份, 请运行以下命令：

```
$ xtrabackup --prepare --apply-log-only --target-dir=/data/backups/base \
--incremental-dir=/data/backups/inc1
```

这会将增量文件应用于 `/data/backups/base` 中的文件, 这会将它们及时向前滚动到增量备份的时间. 然后, 它像往常一样将重做日志应用于结果. 最终数据位于 `/data/backups/base` 中, 而不位于增量目录中. 您应该看到类似于以下内容的输出：

```
incremental backup from 1626007 is enabled.
xtrabackup: cd to /data/backups/base
xtrabackup: This target seems to be already prepared with --apply-log-only.
xtrabackup: xtrabackup_logfile detected: size=2097152, start_lsn=(4124244)
...
xtrabackup: page size for /tmp/backups/inc1/ibdata1.delta is 16384 bytes
Applying /tmp/backups/inc1/ibdata1.delta to ./ibdata1...
...
161011 12:45:56 completed OK!
```

同样, LSN应该与您先前对第一个增量备份的检查中看到的相符. 如果从 `/data/backups/base` 还原文件, 则应该在第一次增量备份时看到数据库的状态. 

::: tip Warning

Percona XtraBackup 不支持使用相同的增量备份目录来准备备份的两个副本. 不要使用相同的增量备份目录（–incremental-dir的值）多次运行--prepare. 
:::

准备第二个增量备份是一个类似的过程：将增量应用到（已修改的）基础备份, 您将及时将其数据前滚到第二个增量备份的点：

```
$ xtrabackup --prepare --target-dir=/data/backups/base \
--incremental-dir=/data/backups/inc2
```

::: tip Note

合并除最后一个以外的所有增量文件时, 应使用 `--apply-log-only` . 这就是为什么上一行不包含 `--apply-log-only` 选项的原因. 即使在最后一步使用了 `--apply-log-only` , 备份仍将保持一致, 但在这种情况下, 服务器将执行回滚阶段. 
:::

一旦准备好增量备份就与完整备份相同, 可以用相同的方式还原它们. 
