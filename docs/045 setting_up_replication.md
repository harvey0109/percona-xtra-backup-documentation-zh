# 如何使用Percona XtraBackup通过6个简单步骤设置要复制的副本

到目前为止，数据是一个系统中最有价值的部分。有系统地进行备份，并在发生故障时可迅速恢复，这对一个系统来说无疑是至关重要的。然而，由于其成本、所需的基础设施或甚至与任务相关的无聊，它并不是常见的做法。Percona XtraBackup就是为了解决这个问题而设计的。

通过使用Percona XtraBackup设置复制环境，您可以通过6个简单的步骤进行几乎实时的备份。

## 您将需要的所有东西

用Percona XtraBackup设置复制的副本是一个简单的程序。为了简单起见，这里列出了你所需要的东西，让你没有麻烦地按照步骤进行。

**Source**

一个安装、配置和运行了基于MySQL的服务器的系统。这个系统将被称为 "源"，因为它是你的数据被存储的地方，也是要被复制的地方。我们将对这个系统做如下假设。

- MySQL服务器能够通过标准的TCP/IP端口与他人通信。
- SSH服务器已经安装和配置。
- 你在系统中拥有一个具有适当权限的用户账户。
- 你有一个具有适当权限的MySQL的用户账户。
- 服务器已启用binlogs，server-id设置为1。

**Replica**

另一个系统，上面安装了一个基于MySQL的服务器。我们把这台机器称为Replica，我们将假设与Source相同的事情，除了Replica上的server-id是2。

**Percona XtraBackup**

我们将使用的备份工具。为方便起见，它应该安装在两台计算机上。

笔记
不建议在复制设置中混合使用MySQL变体（Percona Server，MySQL）。添加新副本时，这可能会产生不正确的xtrabackup_slave_info文件。

## 步骤1：在源上进行备份并进行准备

在源代码处，向shell发出以下命令：

```
$ xtrabackup --backup --user=yourDBuser --password=MaGiCdB1 --target-dir=/path/to/backupdir
```

完成此操作后，您应该获得：

```
xtrabackup: completed OK!
```

这将使你的MySQL数据dir复制到/path/to/backupdir目录。你已经告诉Percona XtraBackup使用你的数据库用户和密码连接到数据库服务器，并对其中的所有数据（所有MyISAM、InnoDB表和其中的索引）做了热备份。

为了使快照一致，你需要在源头上准备数据。

```
$ xtrabackup --user=yourDBuser --password=MaGiCdB1 \
            --prepare --target-dir=/path/to/backupdir
```

你需要选择拍摄快照的路径。如果一切正常，你应该得到同样的确定信息。现在，交易日志被应用于数据文件，并创建了新的数据文件：你的数据文件已经准备好被MySQL服务器使用。

Percona XtraBackup通过读取你的my.cnf知道你的数据在哪里。如果你的配置文件在一个非标准的地方，你应该使用标志--defaults-file =/location/of/my.cnf。

如果你想在每次访问MySQL时跳过写用户名和密码，你可以在.mylogin.cnf中进行如下设置。

```
mysql_config_editor set --login-path=client --host=localhost --user=root --password
```

欲了解更多信息，请参见MySQL配置工具<https://dev.mysql.com/doc/refman/8.0/en/mysql-config-editor.html>。

这将为您提供对MySQL的root访问权限。

## 步骤2：将备份的数据复制到副本服务器

在源端，使用rsync或scp将数据从源端复制到复制端。如果你直接将数据同步到副本的数据目录，我们建议你在那里停止mysqld。

```
$ rsync -avpP -e ssh /path/to/backupdir Replica:/path/to/mysql/
```

复制数据后，您可以备份原始或先前安装的MySQL数据目录（注意：在移动mysqld的数据目录的内容或将快照移至其数据目录之前，请确保mysqld已关闭。）。在副本服务器上运行以下命令：

```
$ mv /path/to/mysql/datadir /path/to/mysql/datadir_bak
```

并将快照从“源”移到其位置：

```
$ xtrabackup --move-back --target-dir=/path/to/mysql/backupdir
```

复制数据后，请确保副本MySQL具有访问它们的适当权限。

```
$ chown mysql:mysql /path/to/mysql/datadir
```

如果ibdata和iblog文件位于datadir之外的目录中，你必须在应用日志后把这些文件放到适当的位置。

## 步骤3：配置来源的MySQL服务器

在源码上，运行以下命令来添加适当的授予。这个授予允许副本能够连接到源文件。

```
> GRANT REPLICATION SLAVE ON *.*  TO 'repl'@'$replicaip'
IDENTIFIED BY '$replicapass';
```

还要确保防火墙规则是正确的，而且 Replica 可以连接到 Source。在 Replica 上运行以下命令，测试是否可以在 Replica 上运行 mysql 客户端，连接到 Source，并进行验证。

```
$ mysql --host=Source --user=repl --password=$replicapass
```

验证特权。

```
mysql> SHOW GRANTS;

```

## 步骤4：配置副本服务器的MySQL服务器

将my.cnf文件从源复制到副本服务器：

```
$ scp user@Source:/etc/mysql/my.cnf /etc/mysql/my.cnf
```

并在/etc/mysql/my.cnf中更改以下选项：

```
server-id=2
```

并在副本服务器上启动/重新启动mysqld。

如果你在基于 Debian 的系统上使用 init 脚本来启动 mysqld，请确保 debian-sys-maint 用户的密码已经更新，并且与该用户在源代码上的密码相同。密码可以在/etc/mysql/debian.cnf中看到并更新。

## 步骤5：开始复制

在Replica上，查看xtrabackup_binlog_info文件的内容，它将是这样的。

```
 $ cat /var/lib/mysql/xtrabackup_binlog_info
Source-bin.000001     481
```

如果您使用的是8.0.23或更高版本，请在副本服务器上执行CHANGE_REPLICATION_SOURCE_TO和MySQL控制台上的相应选项。从该版本开始，CHANGE_MASTER_TO已被弃用。使用您在STEP 3中创建的用户名和密码。

如果你使用的是8.0.23之前的版本，在Replica上，在MySQL控制台执行CHANGE MASTER语句，使用你在步骤3中设置的用户名和密码。

```
> CHANGE MASTER TO
     MASTER_HOST='$sourceip',
     MASTER_USER='repl',
     MASTER_PASSWORD='$replicapass',
     MASTER_LOG_FILE='Source-bin.000001',
     MASTER_LOG_POS=481;
```

并启动副本：

```
> START SLAVE;
```

如果你使用的是 8.0.22 或更高版本，请使用 `START REPLICA `而不是 START SLAVE。START SLAVE 在该版本中已被废弃。如果你使用的是8.0.22之前的版本，请使用START SLAVE。

## 步骤6：检查

在副本服务器上，检查是否一切正常：

```
> SHOW SLAVE STATUS \G
   ...
   Slave_IO_Running: Yes
   Slave_SQL_Running: Yes
   ...
   Seconds_Behind_Master: 13
         ...
```

IO和SQL线程都需要运行。Seconds_Behind_Master意味着当前正在执行的SQL的当前时间戳是13秒前。它是对Source和Replica之间滞后的估计。请注意，在开始时，可能会显示一个高值，因为Replica必须 "赶上 "Source。

## 向源添加更多副本

你可以使用这个程序，稍作改动，向一个源添加新的副本。我们将使用Percona XtraBackup来克隆一个已经配置好的副本。为了方便起见，我们将继续使用之前的方案，但我们将在情节中添加一个NewReplica。

在副本服务器上，执行完整备份：

```
$ xtrabackup --user=yourDBuser --password=MaGiCiGaM \
   --backup --slave-info --target-dir=/path/to/backupdir
```

通过使用--slave-info，Percona XtraBackup可创建名为xtrabackup_slave_info的其他文件。

应用日志：

```
$ xtrabackup --prepare --use-memory=2G --target-dir=/path/to/backupdir/
```

将目录从Replica复制到NewReplica（注意：在将快照内容复制到其数据目录之前，确保NewReplica上的mysqld已经关闭）。

```
rsync -avprP -e ssh /path/to/backupdir NewReplica:/path/to/mysql/datadir
```

例如，要设置一个新的用户，user2，你要在Source上添加一个额外的授予。

```
> GRANT REPLICATION SLAVE ON *.*  TO 'user2'@'$newreplicaip'
 IDENTIFIED BY '$replicapass';
```

在NewReplica上，从副本服务器复制配置文件：

```
$ scp user@Replica:/etc/mysql/my.cnf /etc/mysql/my.cnf
```

确保将/etc/mysql/my.cnf中的server-id变量更改为3并在启动时禁用复制：

```
skip-slave-start
server-id=3
```

设置server_id之后，启动mysqld。

从文件xtrabackup_slave_info中获取master_log_file和master_log_pos，执行为NewReplica设置源和日志文件的语句。

```
> CHANGE MASTER TO
     MASTER_HOST='$Sourceip',
     MASTER_USER='repl',
     MASTER_PASSWORD='$replicapass',
     MASTER_LOG_FILE='Source-bin.000001',
     MASTER_LOG_POS=481;
```

如果你使用8.0.23或更高版本，请使用CHANGE_REPLICATION_SOURCE_TO和适当的选项。从该版本开始， CHANGE_MASTER_TO 已经被废弃。在 8.0.23 之前的版本中，请使用 CHANGE MASTER TO。

并启动副本：

```
> START SLAVE;
```

如果你使用的是 8.0.22 或更高版本，请使用 START REPLICA 而不是 START SLAVE。START SLAVE 在该版本中已被废弃。如果你使用的是8.0.22之前的版本，请使用START SLAVE。

如果当你检查NewReplica时，IO和SQL线程都在运行，那么服务器正在复制Source。