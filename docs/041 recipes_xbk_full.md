# 进行完整备份

将位于/var/lib/mysql/的InnoDB数据和日志文件备份到/data/backups/mysql/（目的地）。然后，准备好备份文件，以备恢复或使用（使数据文件一致）。

进行备份

```
$ xtrabackup --backup --target-dir=/data/backup/mysql/
```

两次准备备份

```
$ xtrabackup --prepare --target-dir=/data/backup/mysql/
$ xtrabackup --prepare --target-dir=/data/backup/mysql/
```

成功标准

- xtrabackup的退出状态为0。
- 在第二个-准备步骤中，你应该看到InnoDB打印类似于`Log file ./ib_logfile0 did not exist: new to be created`，后面有一行表示日志文件已经创建（创建新的日志是第二个准备的目的）。

::: tip Note
如果你在一个有足够空闲内存的专用服务器上，你可能想把 --use-memory 选项设置成一个接近缓冲池大小的值。更多的细节在这里。
更详细的解释在[here](https://www.percona.com/doc/percona-xtrabackup/LATEST/backup_scenarios/full_backup.html#creating-a-backup).
:::