# 节流备份

尽管xtrabackup不会阻碍你的数据库的运行，但任何备份都会给被备份的系统增加负荷。在没有太多剩余I/O容量的系统上，节制xtrabackup读写数据的速度可能会有帮助。你可以用--throttle选项来做这件事。这个选项限制了每秒复制的数据块的数量。大块+大小为10MB。

下图显示了当--throttle被设置为1时节流的工作情况。

![](https://www.percona.com/doc/percona-xtrabackup/LATEST/_images/throttle.png)

与--backup选项一起指定时，此选项限制xtrabackup每秒执行的读写操作对的数量。如果要创建增量备份，则限制为每秒读取I / O操作数。

默认情况下，没有节流，xtrabackup会尽可能快地读写数据。如果你对IOPS的限制太严格，备份可能会很慢，以至于永远赶不上InnoDB正在写入的交易日志，所以备份可能永远不会完成。

