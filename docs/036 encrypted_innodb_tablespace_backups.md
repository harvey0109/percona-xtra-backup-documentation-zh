# 加密的InnoDB表空间备份

从MySQL 5.7开始，InnoDB支持对存储在逐个文件表空间的InnoDB表进行数据加密。该功能为物理表空间数据文件提供静态加密。

或经过认证的用户或应用程序访问一个加密的表空间，InnoDB将使用主加密密钥来解密表空间密钥。主加密密钥存储在一个密钥环中。xtrabackup支持两个密钥环插件：keyring_file，和keyring_vault。这些插件被安装到插件目录中。

- 进行备份
  - 使用keyring_file插件
  - 使用keyring_vault插件
  - 使用keyring_file进行增量加密的InnoDB表空间备份
- 当密匙环不可用时还原备份

## 进行备份

### 使用keyring_file插件

为了备份并准备一个包含加密的InnoDB表空间的数据库，请指定密钥环文件的路径作为--keyring-file-data选项的值。

```
$ xtrabackup --backup --target-dir=/data/backup/ --user=root \
--keyring-file-data=/var/lib/mysql-keyring/keyring
```

::: tip Note

为了与5.7.13之前的MySQL一起使用，在备份创建命令中添加了--server-id选项，使前面的示例如下所示：

```
$ xtrabackup --backup --target-dir=/data/backup/ --user=root \
--keyring-file-data=/var/lib/mysql-keyring/keyring --server-id=1
```

:::

在xtrabackup完成备份后，你应该看到以下信息。

```
xtrabackup: Transaction log of lsn (5696709) to (5696718) was copied.
160401 10:25:51 completed OK!
```

::: tip Warning

xtrabackup不会将钥匙圈文件复制到备份目录中。为了准备备份，你必须自己制作一份钥匙圈文件的副本。

:::

准备备份

为了准备备份，你需要指定keyring-file-data（server-id存储在backup-my.cnf文件中，所以在准备备份时可以省略它，不管使用的是什么MySQL版本）。

```
$ xtrabackup --prepare --target-dir=/data/backup \
--keyring-file-data=/var/lib/mysql-keyring/keyring
```

在xtrabackup完成准备备份后，你应该看到以下信息。

```
InnoDB: Shutdown completed; log sequence number 5697064
160401 10:34:28 completed OK!
```

现在备份已经准备好了，可以用`[`--copy-back`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-copy-back)`选项进行恢复。如果钥匙圈被旋转了，你需要恢复用于获取和准备备份的钥匙圈。

### 使用keyring_vault插件

在Percona XtraBackup 2.4.11中已经实现了对使用keyring_vault的加密InnoDB表空间备份的支持。这里描述了Keyring vault插件的设置。

**创建备份**

命令如

```
$ xtrabackup --backup --target-dir=/data/backup --user=root
```

将在/ data / backup目录中创建一个备份。

在xtrabackup完成备份后，你应该看到以下信息。

```
xtrabackup: Transaction log of lsn (5696709) to (5696718) was copied.
160401 10:25:51 completed OK!
```

**准备备份**

为了准备备份，xtrabackup将需要访问密钥环。由于xtrabackup不会与MySQL服务器通信，并且在准备过程中不会读取默认的my.cnf配置文件，因此用户需要通过命令行指定密钥环设置：

```
$ xtrabackup --prepare --target-dir=/data/backup \
--keyring-vault-config=/etc/vault.cnf
```

::: tip

请在[此处](https://www.percona.com/doc/percona-server/LATEST/management/data_at_rest_encryption.html)查看密钥环库插件设置的说明。

:::

xtrabackup完成备份准备后，您应该看到以下消息：

```
InnoDB: Shutdown completed; log sequence number 5697064
160401 10:34:28 completed OK!
```

现在已准备好备份，可以使用--copy-back选项还原该备份：

```
$ xtrabackup --copy-back --target-dir=/data/backup --datadir=/data/mysql
```

### 使用keyring_file进行增量加密的InnoDB表空间备份

使用InnoDB表空间加密进行增量备份的过程与使用未加密表空间进行增量备份的过程类似。

创建增量备份

要做增量备份，首先要做一个完整的备份。xtrabackup二进制文件将一个名为xtrabackup_checkpoints的文件写入备份的目标目录中。这个文件包含一行显示to_lsn，也就是备份结束时数据库的LSN。首先，你需要用以下命令创建一个完整的备份。

```
$ xtrabackup --backup --target-dir=/data/backups/base \
--keyring-file-data=/var/lib/mysql-keyring/keyring
```

警告
xtrabackup不会将钥匙圈文件复制到备份目录中。为了准备备份，你必须自己制作一份钥匙圈文件的副本。如果你在钥匙圈被修改后试图恢复备份，你会看到类似ERROR 3185 (HY000)的错误。无法从钥匙圈中找到主钥匙，请检查钥匙圈插件是否已加载。当试图访问加密表时。

如果你看一下xtrabackup_checkpoints文件，你应该看到类似以下的内容。

```
backup_type = full-backuped
from_lsn = 0
to_lsn = 7666625
last_lsn = 7666634
compact = 0
recover_binlog_info = 1
```

现在你有一个完整的备份，你可以在此基础上做一个增量备份。使用下面这样的命令。

```
$ xtrabackup --backup --target-dir=/data/backups/inc1 \
--incremental-basedir=/data/backups/base \
--keyring-file-data=/var/lib/mysql-keyring/keyring
```

警告
xtrabackup不会将钥匙圈文件复制到备份目录中。为了准备备份，你必须自己制作一个钥匙圈文件的副本。如果钥匙圈没有被旋转过，你可以使用与你在基本备份中所备份的钥匙圈相同的钥匙圈。如果钥匙圈已经被旋转过，你就需要对它进行备份，否则你将无法准备备份。

/data/backups/inc1/目录现在应该包含delta文件，比如ibdata1.delta和test/table1.ibd.delta。这些代表了自LSN 7666625以来的变化。如果你检查这个目录中的xtrabackup_checkpoints文件，你应该看到与下面类似的内容。

```
backup_type = incremental
from_lsn = 7666625
to_lsn = 8873920
last_lsn = 8873929
compact = 0
recover_binlog_info = 1
```

其含义应该是不言自明的。现在可以用这个目录作为另一个增量备份的基础了。

```
$ xtrabackup --backup --target-dir=/data/backups/inc2 \
--incremental-basedir=/data/backups/inc1 \
--keyring-file-data=/var/lib/mysql-keyring/keyring
```

准备增量备份

增量备份的[`--prepare`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-prepare)步骤与普通备份的-准备步骤不同。在普通备份中，为了使数据库保持一致，会进行两种类型的操作：已提交的事务会从日志文件中针对数据文件进行重放，未提交的事务会被回滚。在准备备份时，你必须跳过回滚未提交的事务，因为在你备份时未提交的事务可能正在进行中，而且很可能在下一次增量备份中提交。你应该使用 --apply-log-only 选项来防止回滚阶段。

警告
如果你不使用 --apply-log-only 选项来防止回滚阶段，那么你的增量备份将毫无作用。在事务被回滚后，不能再应用增量备份。

从你创建的完整备份开始，你可以准备它，然后对它应用增量差异。回顾一下，你有以下的备份。

```
/data/backups/base
/data/backups/inc1
/data/backups/inc2
```

要准备基础备份，你需要像往常一样运行--准备，但要防止回滚阶段。

```
$ xtrabackup --prepare --apply-log-only --target-dir=/data/backups/base \
--keyring-file-data=/var/lib/mysql-keyring/keyring
```

输出应以一些文本结尾，例如：

```
InnoDB: Shutdown completed; log sequence number 7666643
InnoDB: Number of pools: 1
160401 12:31:11 completed OK!
```

要将第一个增量备份应用于全量备份，你应该使用以下命令。

```
$ xtrabackup --prepare --apply-log-only --target-dir=/data/backups/base \
--incremental-dir=/data/backups/inc1 \
--keyring-file-data=/var/lib/mysql-keyring/keyring
```

警告
备份时应使用进行备份时使用的钥匙圈。这意味着，如果钥匙圈在基本备份和增量备份之间被轮换过，你需要使用第一次增量备份时使用的钥匙圈。

准备第二个增量备份也是一个类似的过程：对（修改过的）基础备份应用deltas，你将把它的数据向前滚动到第二个增量备份的位置。

```
$ xtrabackup --prepare --target-dir=/data/backups/base \
--incremental-dir=/data/backups/inc2 \
--keyring-file-data=/var/lib/mysql-keyring/keyring
```

注意事项
--apply-log-only应该在合并除最后一个以外的所有增量时使用。这就是为什么前一行没有包含--apply-log-only选项。即使在最后一步使用--apply-log-only，备份仍然是一致的，但在这种情况下，服务器会执行回滚阶段。

现在备份已经准备好了，可以用[`--copy-back`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-copy-back)恢复选项来恢复。如果钥匙圈被旋转了，你需要恢复用于获取和准备备份的钥匙圈。

### 当密匙环不可用时还原备份

虽然所描述的恢复方法是有效的，但它需要访问服务器正在使用的同一个钥匙圈。如果备份是在不同的服务器上准备的，或者是在更晚的时候准备的，当钥匙圈中的钥匙被清除时，或者在发生故障的情况下，当钥匙圈库服务器根本无法使用时，这可能是不可能的。

应该使用 --transition-key=<passphrase> 选项，使xtrabackup可以在不访问钥匙库服务器的情况下处理备份。在这种情况下，xtrabackup从指定的口令中导出AES加密密钥，并将使用它来加密正在备份的表空间的密钥。

使用密码短语创建备份

以下示例说明了在这种情况下如何创建备份：

```
xtrabackup --backup --user=root -p --target-dir=/data/backup \
--transition-key=MySecetKey
```

如果 --transition-key被指定为没有值，xtrabackup会要求它。

注意
xtrabackup刮除了 --transition-key，所以它的值在ps命令输出中不可见。

使用密码准备备份

应为prepare命令指定相同的口令。

```
xtrabackup --prepare --target-dir=/data/backup
```

这里没有--keyring-vault...或--keyring-file...选项，因为xtrabackup在这种情况下不会与钥匙圈对话。

使用生成的密钥还原备份

当恢复备份时，你将需要生成一个新的主密钥。这里是keyring_file的例子。

```
$ xtrabackup --copy-back --target-dir=/data/backup --datadir=/data/mysql \
--transition-key=MySecetKey --generate-new-master-key \
--keyring-file-data=/var/lib/mysql-keyring/keyring
```

如果是keyring_vault，它将如下所示：

```
$ xtrabackup --copy-back --target-dir=/data/backup --datadir=/data/mysql \
--transition-key=MySecetKey --generate-new-master-key \
--keyring-vault-config=/etc/vault.cnf
```

xtrabackup将生成一个新的主密钥，将其存储在目标密钥库服务器中，并使用这个密钥重新加密表空间的密钥。

使用存储的过渡密钥进行备份

最后，有一个选项是在钥匙圈中存储一个过渡钥匙。在这种情况下，xtrabackup将需要在准备和复制备份时访问相同的钥匙圈文件或金库服务器，但不取决于服务器钥匙是否已被清除。

在这种情况下，备份过程的三个阶段如下所示。

- 备份

```
$ xtrabackup --backup --user=root -p --target-dir=/data/backup \
--generate-transition-key

.. warning::

   This usage of the :option:`--generate-transition-key` option is only
   applicable if |Percona XtraBackup| is used with |Percona Server| version
   lower than 8.0.15-6. For |Percona Server| versions higher than 8.0.15-6,
   :option:`--generate-transition-key` should not be used for making full
   backups with the `keyring_file` plugin.
```

- 准备

  - keyring_file变体：

    ```
    $ xtrabackup --prepare --target-dir=/data/backup \
    --keyring-file-data=/var/lib/mysql-keyring/keyring
    ```

  - keyring_vault变体：

    ```
    $ xtrabackup --prepare --target-dir=/data/backup \
    --keyring-vault-config=/etc/vault.cnf
    ```

- Copy-back

  - keyring_file变体：

    ```
    $ xtrabackup --copy-back --target-dir=/data/backup --datadir=/data/mysql \
    --generate-new-master-key --keyring-file-data=/var/lib/mysql-keyring/keyring
    ```

  - keyring_vault变体

    ```
    $ xtrabackup --copy-back --target-dir=/data/backup --datadir=/data/mysql \
    --generate-new-master-key --keyring-vault-config=/etc/vault.cnf
    ```

    