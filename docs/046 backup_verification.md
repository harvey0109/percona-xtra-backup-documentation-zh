# 使用复制和pt校验和验证备份

验证备份是否一致的一个方法是通过设置复制和运行pt-table-checksum。这可以用来验证任何类型的备份，但是在设置复制之前，备份应该准备好并能够运行（这意味着增量备份应该合并到全量备份，加密备份应该解密等等）。

## 设置复制

如何在6个简单的步骤中用Percona XtraBackup设置复制，指南提供了一个详细的说明，说明如何进行备份和设置复制。

为了检查备份的一致性，你可以使用进行备份的原始服务器，或者使用不同的备份方法（如冷备份、mysqldump或LVM快照）创建的另一个测试服务器作为复制设置中的源服务器。

## 使用pt-table-checksum

这个工具是Percona工具包的一部分。它通过在源上执行检查和查询来执行在线复制一致性检查，在与源不一致的复制上产生不同的结果。

在你确认复制已经设置成功后，你可以安装或下载pt-table-checksum。这个例子显示了下载pt-table-checksum的最新版本。

```
$ wget percona.com/get/pt-table-checksum
```

注意事项
为了使pt-table-checksum正常工作，在Debian/Ubuntu系统上需要安装libdbd-mysql-perl，在RHEL/CentOS上需要安装perl-BD-MySQL。如果你从Percona资源库中安装了percona-toolkit包，包管理器应该自动安装这些库。

运行此命令后，pt-table-checksum将被下载到你的当前工作目录。

```
$ ./pt-table-checksum
    TS ERRORS  DIFFS     ROWS  CHUNKS SKIPPED    TIME TABLE
    04-30T11:31:50      0      0   633135       8       0   5.400 exampledb.aka_name
04-30T11:31:52      0      0   290859       1       0   2.692 exampledb.aka_title
Checksumming exampledb.user_info:  16% 02:27 remain
Checksumming exampledb.user_info:  34% 01:58 remain
Checksumming exampledb.user_info:  50% 01:29 remain
Checksumming exampledb.user_info:  68% 00:56 remain
Checksumming exampledb.user_info:  86% 00:24 remain
04-30T11:34:38      0      0 22187768     126       0 165.216 exampledb.user_info
04-30T11:38:09      0      0        0       1       0   0.033 mysql.time_zone_name
04-30T11:38:09      0      0        0       1       0   0.052 mysql.time_zone_transition
04-30T11:38:09      0      0        0       1       0   0.054 mysql.time_zone_transition_type
04-30T11:38:09      0      0        8       1       0   0.064 mysql.user
```

如果DIFFS列中的所有数值都是0，这意味着备份与当前设置一致。

在备份不一致的情况下，pt-table-checksum应该发现差异并指向不匹配的表。下面的例子显示了在已备份的副本上添加新用户，以模拟不一致的备份。

```
mysql> grant usage on exampledb.* to exampledb@localhost identified by 'thisisnewpassword';
```

如果我们现在运行pt-table-checksum，应该可以发现差异

```
$ ./pt-table-checksum
TS ERRORS  DIFFS     ROWS  CHUNKS SKIPPED    TIME TABLE
04-30T11:31:50      0      0   633135       8       0   5.400 exampledb.aka_name
04-30T11:31:52      0      0   290859       1       0   2.692 exampledb.aka_title
Checksumming exampledb.user_info:  16% 02:27 remain
Checksumming exampledb.user_info:  34% 01:58 remain
Checksumming exampledb.user_info:  50% 01:29 remain
Checksumming exampledb.user_info:  68% 00:56 remain
Checksumming exampledb.user_info:  86% 00:24 remain
04-30T11:34:38      0      0 22187768     126       0 165.216 exampledb.user_info
04-30T11:38:09      0      0        0       1       0   0.033 mysql.time_zone_name
04-30T11:38:09      0      0        0       1       0   0.052 mysql.time_zone_transition
04-30T11:38:09      0      0        0       1       0   0.054 mysql.time_zone_transition_type
04-30T11:38:09      1      0        8       1       0   0.064 mysql.user
```

这个输出显示，源和副本的状态并不一致，差异在mysql.user表中。

关于pt-table-checksum提供的不同选项的更多信息可以在pt-table-checksum文档中找到。