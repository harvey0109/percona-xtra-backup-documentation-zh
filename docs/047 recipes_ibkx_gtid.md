# 如何创建新的（或修复损坏的）基于GTID的副本

MySQL 5.6在复制中引入了新的全局事务ID（GTID）支持。在启用了GTID模式的情况下对MySQL和MySQL 5.7的Percona Server进行备份时，Percona XtraBackup会自动将GTID值存储在xtrabackup_binlog_info中。此信息可用于创建新的（或修复损坏的）基于GTID的副本。



## 步骤1：从复制环境，源或副本上的任何服务器进行备份

下面的命令获取一个备份，并将其保存在/data/backups/$TIMESTAMP文件夹中。

```
$ xtrabackup --backup --target-dirs=/data/backups/
```

在目标文件夹中，将有一个名为xtrabackup_binlog_info的文件。这个文件包含二进制日志坐标和GTID信息。

```
$ cat xtrabackup_binlog_info
mysql-bin.000002    1232        c777888a-b6df-11e2-a604-080027635ef5:1-4
```

xtrabackup在进行备份后也会打印出这些信息。

```
xtrabackup: MySQL binlog position: filename 'mysql-bin.000002', position 1232, GTID of the last change 'c777888a-b6df-11e2-a604-080027635ef5:1-4'
```

## 步骤2：准备备份

备份将在源文件上用以下命令准备。

```
$ xtrabackup --prepare --target-dir=/data/backup
```

你需要选择拍摄快照的路径，例如/data/backups/2013-05-07_08-33-33。如果一切正常，你应该得到同样的确定信息。现在，交易日志被应用于数据文件，并创建了新的数据文件：你的数据文件已经准备好被MySQL服务器使用。

## 步骤3：将备份移至目标服务器

使用rsync或scp将数据复制到目标服务器上。如果你直接将数据同步到已经运行的副本的数据目录，建议在那里停止MySQL服务器。

```
$ rsync -avprP -e ssh /path/to/backupdir/$TIMESTAMP NewSlave:/path/to/mysql/
```

在你把数据复制过来之后，确保MySQL有适当的权限来访问它们。

```
$ chown mysql:mysql /path/to/mysql/datadir
```

## 步骤4：配置并开始复制

将gtid_purged变量设置为xtrabackup_binlog_info的GTID。然后，更新源节点的信息，最后，启动副本。如果你使用的是8.0.22之前的版本，在副本上运行以下命令。

```
# Using the mysql shell
 > SET SESSION wsrep_on = 0;
 > RESET MASTER;
 > SET SESSION wsrep_on = 1;
 > SET GLOBAL gtid_purged='<gtid_string_found_in_xtrabackup_binlog_info>';
 > CHANGE MASTER TO
             MASTER_HOST="$masterip",
             MASTER_USER="repl",
             MASTER_PASSWORD="$slavepass",
             MASTER_AUTO_POSITION = 1;
 > START SLAVE;
```

如果你使用的是 8.0.22 或更高版本，请使用 START REPLICA 而不是 START SLAVE。START SLAVE 在该版本中已被废弃。如果你正在使用8.0.21或更早的版本，请使用START SLAVE。

如果你使用的是8.0.23或更高版本，运行以下命令。

```
# Using the mysql shell
 > SET SESSION wsrep_on = 0;
 > RESET MASTER;
 > SET SESSION wsrep_on = 1;
 > SET GLOBAL gtid_purged='<gtid_string_found_in_xtrabackup_binlog_info>';
 > CHANGE REPLICATION SOURCE TO
             SOURCE_HOST="$masterip",
             SOURCE_USER="repl",
             SOURCE_PASSWORD="$slavepass",
             SOURCE_AUTO_POSITION = 1;
 > START REPLICA;
```

如果你使用8.0.23或更高版本，请使用CHANGE_REPLICATION_SOURCE_TO和适当的选项。从该版本开始，CHANGE_MASTER_TO已经被废弃。

注意事项
上面的例子适用于|PXC|。在重置信号源（RESET MASTER）之前，wsrep_on变量被设置为0。原因是，如果wsrep_on=1，|PXC|将不允许重置源。

## 步骤5：检查复制状态

以下命令将显示副本状态：

```
> SHOW SLAVE STATUS\G
        [..]
        Slave_IO_Running: Yes
        Slave_SQL_Running: Yes
        [...]
        Retrieved_Gtid_Set: c777888a-b6df-11e2-a604-080027635ef5:5
        Executed_Gtid_Set: c777888a-b6df-11e2-a604-080027635ef5:1-5
```

我们可以看到，副本已经检索到一个编号为5的新事务，所以从1到5的事务都已经在这个从机上了。

我们在基于GTID的复制环境中创建了一个新的副本。