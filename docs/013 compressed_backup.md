# 压缩备份

<!-- 已检查 -->

Percona XtraBackup 支持压缩备份:   可以使用[xbstream](https://www.percona.com/doc/percona-xtrabackup/LATEST/glossary.html#term-xbstream)压缩或解压缩本地备份或流式备份.  

## 创建压缩备份

为了进行压缩备份, 您需要使用 [`--compress`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-compress) 选项:   

```
xtrabackup --backup --compress --target-dir=/data/compressed/
```

[`--compress`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-compress) 使用可以通过 `percona-release` 软件包配置工具安装的 `qpress` 工具, 如下所示:   

```bash
sudo percona-release enable tools
sudo apt-get update
sudo apt-get install qpress
```

::: tip Note

启用存储库:   `percona-release enable-only tools release`

如果打算将 Percona XtraBackup 与上游 MySQL Server 结合使用, 则只需启用 `tools` 存储库:   `percona-release enable-only tools` .
::: 

参见 [从存储库安装 Percona XtraBackup](https://www.percona.com/doc/percona-xtrabackup/LATEST/installation.html#installing-from-binaries)

如果要加快压缩速度, 则可以使用并行压缩, 可以使用 [`--compress-threads`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-compress-threads)选项启用它.  以下示例将使用四个线程进行压缩:   

```bash
xtrabackup --backup --compress --compress-threads=4 \
--target-dir=/data/compressed/
```

输出应如下所示

```
...
170223 13:00:38 [01] Compressing ./test/sbtest1.frm to /tmp/compressed/test/sbtest1.frm.qp
170223 13:00:38 [01]        ...done
170223 13:00:38 [01] Compressing ./test/sbtest2.frm to /tmp/compressed/test/sbtest2.frm.qp
170223 13:00:38 [01]        ...done
...
170223 13:00:39 [00] Compressing xtrabackup_info
170223 13:00:39 [00]        ...done
xtrabackup: Transaction log of lsn (9291934) to (9291934) was copied.
170223 13:00:39 completed OK!
```

## 准备备份

准备备份之前, 需要解压缩所有文件.   Percona XtraBackup 已实现[`--decompress`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-decompress) 选项, 可用于解压缩备份.  

```bash
xtrabackup --decompress --target-dir=/data/compressed/
```

::: tip Note

[`--parallel`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-parallel)可与[`--decompress`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-decompress)选项一起使用, 以同时解压缩多个文件.  
::: 

Percona XtraBackup 不会自动删除压缩文件.  为了清理备份目录, 您应该使用 [`--remove-original`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-remove-original) 选项.  即使未删除这些文件, 如果使用 [`--copy-back`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-copy-back) 或 [`--move-back`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-move-back), 这些文件也不会被 复制/移动 到datadir中.  

解压缩文件后, 可以使用[`--prepare`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-prepare)选项准备备份:   

```bash
xtrabackup --prepare --target-dir=/data/compressed/
```





您应该检查确认消息:   

```
InnoDB: Starting shutdown...
InnoDB: Shutdown completed; log sequence number 9293846
170223 13:39:31 completed OK!
```



现在, `/data/compressed/`中的文件已准备就绪, 可以由服务器使用.  

## 恢复备份

xtrabackup具有[`--copy-back`](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#cmdoption-copy-back)选项, 该选项将备份还原到服务器的[`数据目录`](https://www.percona.com/doc/percona-xtrabackup/LATEST/glossary.html#term-datadir):   

```bash
xtrabackup --copy-back --target-dir=/data/backups/
```

它将所有与数据相关的文件复制回服务器的datadir, 该文件由服务器的`my.cnf`配置文件确定.  您应该检查输出的最后一行以获取成功消息:   

```
170223 13:49:13 completed OK!
```

将数据复制回后, 应检查文件权限.  您可能需要用以下方法调整它们:   

```bash
chown -R mysql:mysql /var/lib/mysql
```

现在, [`datadir`]() 包含还原的数据.  您已准备好启动服务器.  
