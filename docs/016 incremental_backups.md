# 增量备份

xtrabackup 支持增量备份.    它仅复制自上次完整备份以来已更改的数据.    您可以在每个完整备份之间执行许多增量备份,   因此可以设置备份过程,   例如每周一次完整备份和每天增量备份,   或者每天完整备份和每小时增量备份.    

增量备份之所以有效,   是因为每个 InnoDB页（通常为16kb）都包含一个日志序列号或LSN.     LSN 是整个数据库的系统版本号.    每个页面的 LSN 都显示了最近的更改.    增量备份将复制其 LSN 比先前的增量或完全备份的 LSN 更新的每个页面.    有两种算法用于查找要复制的此类页面集.    第一个可用于所有服务器类型和版本的服务器是通过读取所有数据页直接检查 LSN 页.     Percona Server for MySQL 中提供的第二个功能是启用服务器上已更改的页面跟踪功能,   该功能将在更改页面时记录下来.    然后将这些信息写在一个紧凑的单独的所谓的位图文件中.     xtrabackup 二进制文件将使用该文件来读取增量备份所需的数据页,   从而可能保存许多读取请求.    如果 xtrabackup 二进制文件找到了位图文件,   则默认情况下启用后一种算法.    即使位图数据可用,   也可以指定 `--incremental-force-scan` 读取所有页面.    

增量备份实际上不会将数据文件与先前备份的数据文件进行比较.    实际上,   如果您知道它的 LSN ,   则可以使用 `--incremental-lsn` 来执行增量备份,   甚至不需要先前的备份.    增量备份只需阅读页面,   然后将其LSN与上次备份的 LSN 进行比较即可.    但是,   您仍然需要完整备份来恢复增量更改.    没有完整的备份作为基础,   增量备份将毫无用处.    

## 创建增量备份

要进行增量备份,   请像往常一样从完整备份开始.     xtrabackup 二进制文件将名为 xtrabackup_checkpoints 的文件写入备份的目标目录.    该文件包含一行显示 to_lsn 的行,   该行是备份结束时数据库的 LSN .    使用以下命令创建完整备份：

```
$ xtrabackup --backup --target-dir=/data/backups/base --datadir=/var/lib/mysql/
```
如果查看xtrabackup_checkpoints文件,   应该看到类似于以下内容：

```
backup_type = full-backuped
from_lsn = 0
to_lsn = 1291135
```

现在您已拥有完整备份,   您可以基于它进行增量备份.    使用如下命令：

```
$ xtrabackup --backup --target-dir=/data/backups/inc1 \
--incremental-basedir=/data/backups/base --datadir=/var/lib/mysql/
```

`/data/backups/inc1/` 目录现在应该包含增量文件,   例如 `ibdata1.delta` 和 `test/table1.ibd.delta` .    这些代表自 `LSN 1291135` 以来的更改.    如果检查此目录中的 xtrabackup_checkpoints 文件,   则应看到类似于以下内容：

```
backup_type = incremental
from_lsn = 1291135
to_lsn = 1291340
```

含义应该是不言而喻的.    现在可以将此目录用作另一个增量备份的基础：

```
$ xtrabackup --backup --target-dir=/data/backups/inc2 \
--incremental-basedir=/data/backups/inc1 --datadir=/var/lib/mysql/
```

## 准备增量备份

增量备份的 `--prepare` 步骤与普通备份不同.    在普通备份中,   执行两种类型的操作以使数据库保持一致：已提交的事务相对于数据文件从日志文件中重放,   未提交的事务被回滚.    准备备份时,   必须跳过未提交事务的回滚,   因为在备份时未提交的事务可能正在进行中,   并且很有可能将在下一个增量备份中提交.    您应该使用 `--apply-log-only `选项来防止回滚阶段.    

::: tip Note
如果不使用 `--apply-log-only` 选项来防止回滚阶段,   则增量备份将无用.    事务回滚后,   无法再应用增量备份.    
::: 

从您创建的完整备份开始,   您可以准备它,   然后将增量差异应用于它.    回想一下,   您有以下备份：

```
/data/backups/base
/data/backups/inc1
/data/backups/inc2
```

要准备基本备份,   您需要像往常一样运行 `--prepare` ,   但要防止回滚阶段：

```
$ xtrabackup --prepare --apply-log-only --target-dir=/data/backups/base
```

输出应以一些文本结尾,   例如：

```
101107 20:49:43  InnoDB: Shutdown completed; log sequence number 1291135
```

日志序列号应与您先前看到的基本备份的 to_lsn 相匹配.    
即使已跳过回滚阶段,   此备份实际上现在也可以安全地按原样还原.    如果还原它并启动MySQL,   InnoDB将检测到未执行回滚阶段,   它将在后台执行该操作,   就像启动时进行崩溃恢复一样.    它将通知您数据库未正常关闭.    

要将第一个增量备份应用于完整备份,   应使用以下命令：

```
$ xtrabackup --prepare --apply-log-only --target-dir=/data/backups/base \
--incremental-dir=/data/backups/inc1
```

这会将增量文件应用于 `/data/backups/base` 中的文件,   这会将它们及时向前滚动到增量备份的时间.    然后,   它像往常一样将重做日志应用于结果.    最终数据位于`/data/backups/base` 中,   而不位于增量目录中.    您应该看到一些输出,   例如以下内容：

```
incremental backup from 1291135 is enabled.
xtrabackup: cd to /data/backups/base/
xtrabackup: This target seems to be already prepared.
xtrabackup: xtrabackup_logfile detected: size=2097152, start_lsn=(1291340)
Applying /data/backups/inc1/ibdata1.delta ...
Applying /data/backups/inc1/test/table1.ibd.delta ...
.... snip
101107 20:56:30  InnoDB: Shutdown completed; log sequence number 1291340
```

同样,   LSN应该与您先前对第一个增量备份的检查中看到的相符.    如果从 `/data/backups/base` 还原文件,   则应该在第一次增量备份时看到数据库的状态.    
准备第二个增量备份是一个类似的过程：将增量应用到（已修改的）基础备份,   您将及时将其数据前滚到第二个增量备份的点：

```
$ xtrabackup --prepare --target-dir=/data/backups/base \
--incremental-dir=/data/backups/inc2
```

::: tip Note
合并除最后一个以外的所有增量文件时,   应使用 --apply-log-only .    这就是为什么上一行不包含--apply-log-only选项的原因.    即使在最后一步使用了 --apply-log-only ,   备份仍将保持一致,   但在这种情况下,   服务器将执行回滚阶段.    
:::

如果您希望避免看到InnoDB没有正常关闭的通知,   则在将所需的增量应用到基本备份时,   可以再次运行 --prepare 而不禁用回滚阶段.    

## 恢复增量备份

准备增量备份后,   基本目录包含与完整备份相同的数据.    要还原此备份,   可以使用以下命令：`xtrabackup --copy-back --target-dir = BASE-DIR`

您可能必须按照还原备份中所述更改所有权.    

## 使用xbstream的增量流备份

可以使用xbstream流选项执行增量流备份.    当前,   备份以自定义 xbstream 格式打包.    使用此功能,   您还需要进行 BASE 备份.    

进行基本备份

```
$ xtrabackup --backup --target-dir=/data/backups
```

进行本地备份

```
$ xtrabackup --backup --incremental-lsn=LSN-number --stream=xbstream --target-dir=./ > incremental.xbstream
```

解压备份

```
$ xbstream -x < incremental.xbstream
```

进行本地备份并将其流式传输到远程服务器并解压缩

```
$ xtrabackup --backup --incremental-lsn=LSN-number --stream=xbstream --target-dir=./
$ ssh user@hostname " cat - | xbstream -x -C > /backup-dir/"
```
