# 服务器版本和备份版本比较

Percona XtraBackup 8.0.21添加了--no-server-version-check参数。此参数将源系统版本与Percona XtraBackup版本进行比较。

该参数检查以下情况：

- 源系统和PXB版本相同，备份继续
- 源系统小于PXB版本，备份继续
- 源系统大于PXB版本，并且该参数未覆盖，备份已停止并返回错误消息
- 源系统大于PXB版本，并且该参数被覆盖，备份继续进行

像示例一样，显式添加`--no-server-version-check`参数将覆盖该参数，备份将继续进行。

```
xtrabackup --backup --no-server-version-check --target-dir=$mysql/backup1
```

当您覆盖参数时，可能发生以下事件：

- 备份失败
- 创建损坏的备份
- 备份成功
