# 使用二进制日志

xtrabackup 二进制文件与 InnoDB 在其事务日志中存储的有关已提交事务的相应二进制日志位置的信息集成在一起。这使它可以打印出备份所对应的二进制日志位置，因此您可以使用它来设置新的复制副本或执行时间点恢复。

## 查找二进制日志位置

准备好备份后，您可以找到与备份相对应的二进制日志位置。这可以通过使用 --prepare 或 --apply-log-only 选项运行 xtrabackup 来完成。如果您的备份来自启用了二进制日志记录的服务器，则 xtrabackup 将在目标目录中创建一个名为 xtrabackup_binlog_info 的文件。该文件包含二进制日志文件的名称和二进制日志中与准备好的备份对应的确切点的位置。

在准备阶段，您还将看到类似于以下内容的输出：

```
InnoDB: Last MySQL binlog file position 0 3252710, file name ./mysql-bin.000001
... snip ...
[notice (again)]
  If you use binary log and don't use any hack of group commit,
  the binary log position seems to be:
InnoDB: Last MySQL binlog file position 0 3252710, file name ./mysql-bin.000001
```

也可以在 xtrabackup_binlog_pos_innodb 文件中找到此输出，但是仅当 XtraDB 或 InnoDB 用作存储引擎时，此输出才正确。

如果使用了其他存储引擎（即MyISAM），则应使用 xtrabackup_binlog_info 文件来检索位置。

有关黑客攻击组提交的消息是指Percona Server for MySQL中的仿真组提交的早期实现。

## 时间点恢复

要从xtrabackup备份执行时间点恢复，您应该准备并还原备份，然后从xtrabackup_binlog_info文件中显示的点重播二进制日志。

在[此处](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/point-in-time-recovery.html#pxb-xtrabackup-point-in-time-recovery)找到更详细的过程。

## 设置新的复制副本

要设置新副本，您应该准备备份，并将其还原到新副本副本的数据目录中。如果使用的是8.0.22或更早版本，请在 CHANGE MASTER TO 命令中，使用 xtrabackup_binlog_info 文件中显示的二进制日志文件名和位置开始复制。

如果您使用的是8.0.23或更高版本，请使用 CHANGE_REPLICATION_SOURCE_TO 和相应的选项。 CHANGE_MASTER_TO已弃用。

在[如何使用Percona XtraBackup通过6个简单的步骤为复制设置副本](https://www.percona.com/doc/percona-xtrabackup/LATEST/howtos/setting_up_replication.html)时，可以找到更详细的过程。
