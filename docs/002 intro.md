# 关于Percona XtraBackup

Percona XtraBackup是世界上唯一的开源免费MySQL热备份软件，可对InnoDB和XtraDB数据库执行非阻塞备份。使用Percona XtraBackup，您可以获得以下好处：

- 备份快速可靠地完成
- 备份期间不间断的事务处理
- 节省磁盘空间和网络带宽
- 自动备份验证
- 更快的恢复时间，更长的正常运行时间

Percona XtraBackup可以对适用于MySQL和MySQL的所有版本的Percona Server进行MySQL热备份。它执行流式，压缩式和增量式MySQL备份。

::: tip 重要
随着Percona XtraBackup 8.0的引入，Percona XtraBackup 2.4将继续支持MySQL和Percona Server 5.6和5.7数据库。由于新的MySQL重做日志和数据字典格式，Percona XtraBackup 8.0.x版本仅与MySQL 8.0.x和即将推出的Percona Server for MySQL 8.0.x兼容。
:::




Percona的企业级商业MySQL支持合同包括对Percona XtraBackup的支持。我们建议为关键的生产部署提供支持。

**支持的存储引擎**

Percona XtraBackup可与MySQL和Percona Server for MySQL一起使用。它支持InnoDB，XtraDB和MyRocks存储引擎的完全非阻塞备份。此外，它还可以通过在备份结束时短暂暂停写入来备份以下存储引擎：MyISAM，Merge和Archive，包括分区表，触发器和数据库选项。

::: tip 重要
版本8.0.6中添加了对MyRocks存储引擎的支持。

Percona XtraBackup 8.0不支持TokuDB存储引擎。

参见[Percona TokuBackup](https://www.percona.com/doc/percona-server/LATEST/tokudb/toku_backup.html)
:::

## Percona XtraBackup的功能是什么？

这是Percona XtraBackup功能的简短列表。有关更多信息，请参见文档

- 创建热InnoDB备份而不会暂停数据库
- 进行MySQL的增量备份
- 将压缩的MySQL备份流式传输到另一台服务器
- 在线在MySQL服务器之间移动表
- 轻松创建新的MySQL复制副本
- 在不增加服务器负载的情况下备份MySQL

**脚注**

复制非InnoDB数据时，InnoDB表仍处于锁定状态。

启用XtraDB更改页面跟踪的Percona Server for MySQL支持快速增量备份。

Percona XtraBackup支持使用任何类型的备份进行加密。 MySQL Enterprise Backup仅支持单文件备份的加密。

Percona XtraBackup根据每秒的IO操作数执行限制。 MySQL Enterprise Backup支持两次操作之间可配置的睡眠时间。

当准备压缩备份时，Percona XtraBackup会跳过二级索引页面并重新创建它们。 MySQL Enterprise Backup跳过未使用的页面，并在准备阶段重新插入。

无论InnoDB版本如何，Percona XtraBackup都可以从完整备份中导出单个表。 MySQL Enterprise Backup仅在执行部分备份时才使用InnoDB 5.6可移植表空间。

备份锁是Percona Server for MySQL中可用的`FLUSH TABLES WITH READ LOCK`的一个轻量级替代方案。 Percona XtraBackup自动使用它们来复制非InnoDB数据，以避免阻止修改InnoDB表的DML查询。

有关更多信息，请参见 [Percona XtraBackup的工作方式](https://www.percona.com/doc/percona-xtrabackup/LATEST/how_xtrabackup_works.html#how-xtrabackup-works)
