# Percona XtraBackup 8.0 Release Notes





- [*Percona XtraBackup* 8.0.23-16.0](release-notes/8.0/8.0.23-16.0.html)
- [*Percona XtraBackup* 8.0.22-15.0](release-notes/8.0/8.0.22-15.0.html)
- [*Percona XtraBackup* 8.0.14](release-notes/8.0/8.0.14.html)
- [*Percona XtraBackup* 8.0.13](release-notes/8.0/8.0.13.html)
- [*Percona XtraBackup* 8.0.12](release-notes/8.0/8.0.12.html)
- [Percona XtraBackup 8.0.11](release-notes/8.0/8.0.11.html)
- [Percona XtraBackup 8.0.10](release-notes/8.0/8.0.10.html)
- [Percona XtraBackup 8.0.9](release-notes/8.0/8.0.9.html)
- [Percona XtraBackup 8.0.8](release-notes/8.0/8.0.8.html)
- [Percona XtraBackup 8.0.7](release-notes/8.0/8.0.7.html)
- [Percona XtraBackup 8.0.6](release-notes/8.0/8.0.6.html)
- [Percona XtraBackup 8.0.5](release-notes/8.0/8.0.5.html)
- [Percona XtraBackup 8.0.4](release-notes/8.0/8.0.4.html)
- [*Percona XtraBackup* 8.0-3-rc1](release-notes/8.0/8.0-3-rc1.html)