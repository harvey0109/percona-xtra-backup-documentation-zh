# 配置xtrabackup

所有xtrabackup配置都是通过选项完成的，这些选项的行为与标准MySQL程序选项完全相同：可以在命令行中指定它们，也可以通过`/etc/my.cnf`等文件来指定它们。


xtrabackup二进制文件从任何配置文件中依次读取`[mysqld]`和`[xtrabackup]`部分。这样一来，它便可以从现有的MySQL安装中读取其选项，例如`datadir`或某些`InnoDB`选项。如果要覆盖它们，则只需在`[xtrabackup]`部分中指定它们，因为稍后将对其进行读取，因此将优先使用它们。

如果您不想这样做，则无需在`my.cnf`中进行任何配置。您可以简单地在命令行上指定选项。通常，在`my.cnf`文件的`[xtrabackup]`部分中可能会发现唯一方便的地方是`target_dir`选项，该选项默认将放置备份的目录，例如：

```
[xtrabackup]
target_dir = /data/backups/mysql/
```
本手册将假定您没有任何用于xtrabackup的基于文件的配置，因此它将始终显示正在显式使用的命令行选项。请参阅[选项和变量参考](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup_bin/xbk_option_reference.html#xbk-option-reference)以获取有关所有配置选项的详细信息。

xtrabackup二进制文件在`my.cnf`文件中不接受与mysqld服务器二进制文件完全相同的语法。由于历史原因，mysqld服务器二进制文件使用`--set-variable = <variable> = <value>`语法接受参数，而xtrabackup无法理解。如果`my.cnf`文件具有此类配置指令，则应使用`--variable = value`语法重写它们。

## 系统配置和NFS卷

在大多数系统上，xtrabackup工具不需要任何特殊配置。但是，调用`fsync（）`时，`--target-dir`所在的存储必须正确运行。特别是，我们注意到，未使用`sync`选项安装的NFS卷可能不会真正同步数据。因此，如果备份到使用`async`选项安装的NFS卷，然后尝试从也安装该卷的另一台服务器准备备份，则数据可能看起来已损坏。您可以使用`sync mount`选项来避免此问题。
