# Percona XtraBackup 用户手册

<!-- 已检查 -->

Percona XtraBackup 包含以下工具：

- xtrabackup 编译的C二进制文件，提供使用 MyISAM，InnoDB 和 XtraDB表 备份整个MySQL数据库实例的功能。
- xbcrypt 用于加密和解密备份文件的实用程序。
- xbstream 该实用程序允许以流的形式从 xbstream 格式提取文件或从 xbstream 格式提取文件。
- xbcloud 实用程序，用于从云中下载全部或部分 xbstream 归档并将其上传到云。

在Percona XtraBackup 2.3发布之后，推荐的备份方式是使用 xtrabackup 脚本。有关脚本选项的更多信息，请参见如何使用xtrabackup。
