# Percona XtraBackup-文档


Percona XtraBackup是适用于MySQL的基于服务器的开源热备份实用程序，在备份过程中不会锁定数据库。

无论是24x7高负载服务器还是低事务量环境，Percona XtraBackup都旨在使备份成为无缝过程，而不会破坏生产环境中服务器的性能。有商业支持合同。

Percona XtraBackup可以备份MySQL 8.0服务器上的InnoDB，XtraDB，MyISAM和MyRocks表中的数据，以及具有XtraDB的MySQL Percona服务器，适用于MySQL 8.0的Percona Server和Percona XtraDB Cluster 8.0的数据。

::: tip Important
版本8.0.6中添加了对MyRocks存储引擎的支持。

Percona XtraBackup 8.0不支持TokuDB存储引擎。

参见: [Percona TokuBackup](https://www.percona.com/doc/percona-server/LATEST/tokudb/toku_backup.html)
:::


Percona XtraBackup 8.0不支持对在MySQL 8.0之前的版本，用于MySQL的Percona Server或Percona XtraDB群集中创建的数据库进行备份。由于MySQL 8.0在数据字典，重做日志和撤消日志中引入的更改与以前的版本不兼容，因此Percona XtraBackup 8.0目前不可能也支持8.0之前的版本。

有关更多信息，请参见[Percona XtraBackup 8.x and MySQL 8.0.20](https://www.percona.com/blog/2020/04/28/percona-xtrabackup-8-x-and-mysql-8-0-20/)

有关其许多高级功能的高级概述，包括功能比较，请参阅关于[Percona XtraBackup](https://www.percona.com/doc/percona-xtrabackup/LATEST/intro.html)。

## 介绍

- [关于Percona XtraBackup](001-AboutPerconaXtraBackup.html)
- [Percona XtraBackup 原理](002-HowPerconaXtraBackupWorks.html)

## 安装

- [安装Percona XtraBackup 8.0](003-InstallingPerconaXtraBackup8.0.md)
    - [从存储库安装Percona XtraBackup]()
    - [从二进制Tarball安装Percona XtraBackup]()
    - [从源代码编译和安装]()
    - [在Docker容器中运行Percona XtraBackup]()

## 前提知识

- [需要连接和特权]()
- [配置xtrabackup]()
- [服务器版本和备份版本比较]()

## 备份方案

- 完整备份
- 增量备份
- 压缩备份

## 用户手册

- Percona XtraBackup 用户手册

## 高级功能

- 节流备份
- 加密的InnoDB表空间备份
- lock-ddl-per-table 选项改进

## Security

- 使用 SELinux
- 使用 AppArmor



## 教程 Recipes 如何使用

- Recipes for xtrabackup
- 如何使用
- 辅助指南

## 参考资料

- xtrabackup选项参考
- xbcloud二进制文件
- xbcrypt二进制文件
- xbstream二进制
- 常问问题
- Percona XtraBackup 8.0 版本说明
- 词汇表
- 由Percona XtraBackup创建的文件索引
- Trademark Policy
- 版本检查

## 索引和表格

- 索引
- 索引
