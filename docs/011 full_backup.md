# 完整备份

<!-- 检查 -->

## 创建备份

要创建备份, 请使用`--backup`选项运行xtrabackup. 您还需要指定`--target-dir`选项, 这是备份的存储位置, 如果InnoDB数据或日志文件未存储在同一目录中, 则可能还需要指定这些文件的位置. 如果目标目录不存在, 则xtrabackup会创建它. 如果该目录确实存在并且为空, 则xtrabackup将成功. 

xtrabackup不会覆盖现有文件, 它将因操作系统错误17而失败, `file exists`. 

要启动备份过程, 请运行: 

```bash
$ xtrabackup --backup --target-dir=/data/backups/
```

这会将备份存储在`/data/backups/`中. 如果指定相对路径, 则目标目录将相对于当前目录. 

在备份过程中, 您应该看到很多输出, 显示正在复制的数据文件, 以及日志文件线程反复扫描日志文件并从中复制. 这是一个示例, 该示例显示了日志线程在后台扫描日志, 以及在ibdata1文件上工作的文件复制线程: 

```
160906 10:19:17 Finished backing up non-InnoDB tables and files
160906 10:19:17 Executing FLUSH NO_WRITE_TO_BINLOG ENGINE LOGS...
xtrabackup: The latest check point (for incremental): '62988944'
xtrabackup: Stopping log copying thread.
.160906 10:19:18 >> log scanned up to (137343534)
160906 10:19:18 Executing UNLOCK TABLES
160906 10:19:18 All tables unlocked
160906 10:19:18 Backup created in directory '/data/backups/'
160906 10:19:18 [00] Writing backup-my.cnf
160906 10:19:18 [00]        ...done
160906 10:19:18 [00] Writing xtrabackup_info
160906 10:19:18 [00]        ...done
xtrabackup: Transaction log of lsn (26970807) to (137343534) was copied.
160906 10:19:18 completed OK!
```

您应该看到的最后一件事类似于以下内容, 其中`<LSN>`的值将是一个取决于您的系统的数字: 

```
xtrabackup: Transaction log of lsn (<SLN>) to (<LSN>) was copied.
```

::: tip Note

日志复制线程每秒检查一次事务日志, 以查看是否有任何新的日志记录需要复制, 但是日志复制线程有可能无法跟上要写入的日志数量. 事务性日志, 当日志记录被覆盖而无法读取时, 将报错. 
:::


备份完成后, 假设您只有一个InnoDB表  `test.tbl1` , 并且使用的是MySQL的 `innodb_file_per_table` 选项, 则目标目录将包含以下文件: 

```
$ ls -lh /data/backups/
total 182M
drwx------  7 root root 4.0K Sep  6 10:19 .
drwxrwxrwt 11 root root 4.0K Sep  6 11:05 ..
-rw-r-----  1 root root  387 Sep  6 10:19 backup-my.cnf
-rw-r-----  1 root root  76M Sep  6 10:19 ibdata1
drwx------  2 root root 4.0K Sep  6 10:19 mysql
drwx------  2 root root 4.0K Sep  6 10:19 performance_schema
drwx------  2 root root 4.0K Sep  6 10:19 sbtest
drwx------  2 root root 4.0K Sep  6 10:19 test
drwx------  2 root root 4.0K Sep  6 10:19 world2
-rw-r-----  1 root root  116 Sep  6 10:19 xtrabackup_checkpoints
-rw-r-----  1 root root  433 Sep  6 10:19 xtrabackup_info
-rw-r-----  1 root root 106M Sep  6 10:19 xtrabackup_logfile
```

备份可能需要很长时间, 具体取决于数据库的大小. 随时可以取消, 因为 xtrabackup 不会修改数据库

下一步是准备好还原您的备份. 

## 准备备份

使用 `--backup` 选项进行备份后, 您需要准备它才能还原它. 在准备好数据文件之前, 它们在时间点上是不一致的, 因为在程序运行时它们是在不同的时间复制的, 并且在此过程中它们可能已被更改. 

如果您尝试使用这些数据文件启动InnoDB, 它将检测到损坏并停止工作以避免在损坏的数据上运行.  `--prepare`步骤可以使文件在单个时刻完美地保持一致, 因此您可以在文件上运行InnoDB. 

您可以在任何计算机上运行 `prepare` 操作；它不必位于原始服务器上或要还原到的服务器上. 您可以将备份复制到实用程序服务器并在那里准备. 

请注意, Percona XtraBackup 8.0 只能准备 MySQL 8.0, Percona Server for MySQL 8.0 和 Percona XtraDB Cluster 8.0 数据库的备份. 不支持8.0之前的版本. 

在准备操作期间, xtrabackup 启动了一种经过修改的嵌入式 InnoDB (xtrabackup库已链接到该库) . 必须进行修改才能禁用 InnoDB 标准安全检查, 例如抱怨日志文件大小不正确. 此警告不适用于备份. 这些修改仅适用于 xtrabackup 二进制文件；您不需要修改的 InnoDB 即可使用 xtrabackup 进行备份. 

准备步骤使用此 `嵌入式InnoDB` 通过复制的日志文件对复制的数据文件执行崩溃恢复.  prepare 步骤的使用非常简单: 您只需使用 `--prepare`选项运行 xtrabackup 并告诉它要准备的目录, 例如, 准备先前进行的备份运行: 

```
$ xtrabackup --prepare --target-dir=/data/backups/
```

完成此操作后, 您应该看到 `InnoDB shutdown` , 并显示以下消息, 其中 LSN 的值将再次取决于您的系统: 

```
InnoDB: Shutdown completed; log sequence number 137345046
160906 11:21:01 completed OK!
```

以下所有准备工作都不会更改已经准备好的数据文件, 您将看到输出显示: 

```
xtrabackup: This target seems to be already prepared.
xtrabackup: notice: xtrabackup_logfile was already used to '--prepare'.
```

不建议在准备备份时中断 xtrabackup 过程, 因为这可能会导致数据文件损坏并且备份将变得不可用. 如果准备过程中断, 则不能保证备份的有效性. 

::: tip Note

如果打算将备份作为进一步增量备份的基础, 则在准备备份时应使用 `--apply-log-only` 选项, 否则将无法对其应用增量备份. 有关更多详细信息, 请参见有关准备[增量备份](https://www.percona.com/doc/percona-xtrabackup/LATEST/backup_scenarios/incremental_backup.html#incremental-backup)的文档. 
:::

## 恢复备份

::: tip Warning

必须先准备好备份, 然后才能还原它. 
::: 


如果您不想保存备份, 则可以使用 `--move-back` 选项, 该选项会将备份的数据移至datadir. 

```bash
$ xtrabackup --copy-back --target-dir=/data/backups/
```

如果您不想使用以上任何选项, 则可以另外使用rsync或cp来还原文件. 

::: tip Note

恢复备份之前, datadir必须为空. 同样需要注意的是, 必须先关闭MySQL服务器, 然后才能执行还原操作. 您不能还原到正在运行的mysqld实例的数据目录 (导入部分备份时除外) . 
::: 


可用于还原备份的rsync命令示例如下所示: 

```bash
$ rsync -avrP /data/backup/ /var/lib/mysql/
```

您应该检查恢复的文件是否具有正确的所有权和权限. 

由于将保留文件的属性, 因此在大多数情况下, 在启动数据库服务器之前, 您需要将文件的所有权更改为 mysql, 因为它们将由创建备份的用户拥有: 

```
$ chown -R mysql:mysql /var/lib/mysql
```

现在, 数据已还原, 您可以启动服务器. 
