# `FLUSH TABLES WITH READ LOCK` 处理方式

进行备份时，将在备份非InnoDB文件之前使用 `FLUSH TABLES WITH READ LOCK`，以确保备份一致。即使可能有一个正在运行的查询已经执行了数小时，`FLUSH TABLES WITH READ LOCK` 仍可以运行。


在这种情况下，所有内容都锁定在 `Waiting for table flush` 或 `Waiting for master to send event` 状态中。使用 `FLUSH TABLES WITH READ LOCK ` 不能解决此问题。使服务器再次正常运行的唯一方法是消除长时间运行的查询，这些查询从一开始就阻止了它。这意味着，如果有长时间运行的查询， `FLUSH TABLES WITH READ LOCK ` 可能会卡住，将服务器置于只读模式，直到等待这些查询完成。

::: tip Note
使用备份锁时，本节中描述的所有内容均无效。 Percona XtraBackup 将在可用的情况下使用备份锁，以替代带 `FLUSH TABLES WITH READ LOCK` 。 Percona Server for MySQL 5.6+中提供了此功能。 Percona XtraBackup 使用此功能自动复制非InnoDB数据，以避免阻止修改InnoDB表的DML查询。
:::

为了防止这种情况的发生，已经实现了两件事：

- xtrabackup 可以等待一个好时机来发出全局锁定。
- xtrabackup 可以杀死所有或只杀死阻碍全局锁获取的SELECT查询。

## 等待查询完成

发出全局锁的好时机是没有长的查询在运行的时候。但是长时间等待一个好的时机来发布全局锁并不总是好的方法，因为它可能会延长备份所需的时间。为了防止xtrabackup等待发布`FLUSH TABLES WITH READ LOCK`的时间过长，已经实施了新的选项：`--ftwrl-wait-timeout`选项，可以用来限制等待时间。如果在这段时间内没有出现发锁的好时机，xtrabackup将放弃，并以错误信息退出，备份将不被采取。该选项的值为零时，将关闭该功能（这是默认值）。

另一种可能性是指定要等待的查询类型。在这种情况下 `--ftwrl-wait-query-type` 。可能的值均为all并更新。使用全部后，xtrabackup将等待所有长时间运行的查询（执行时间长于 `--ftwrl-wait-threshold` 所允许的时间）完成，然后再运行  `FLUSH TABLES WITH READ LOCK`。使用`update`时，xtrabackup将等待`UPDATE/ALTER/REPLACE/INSERT`查询完成。

尽管很难预测完成特定查询所需的时间，但是我们可以假设已经运行很长时间的查询不太可能很快完成。短时间运行的查询可能很快就会完成。 xtrabackup可以使用 `--ftwrl-wait-threshold` 选项的值来指定哪个查询长时间运行，并且可能会阻塞全局锁定一段时间。为了使用此选项，xtrabackup 用户应具有 PROCESS 和 SUPER 特权。

## 杀死阻塞查询

第二种选择是杀死所有阻止获取全局锁的查询。在这种情况下，所有运行时间超过 `FLUSH TABLES WITH READ LOCK` 的查询都是潜在的阻止程序。尽管可以终止所有查询，但是可以使用 `--kill-long-queries-timeout `选项为运行时间较短的查询指定额外的时间。此选项指定查询完成的时间，达到该值后，所有正在运行的查询将被终止。默认值为零，这将关闭此功能。

`--kill-long-query-type` 选项可用于指定所有或仅选择阻止全局锁获取的SELECT查询。为了使用此选项，xtrabackup 用户应具有 PROCESS 和 SUPER 特权。

## 选项摘要

- `--ftwrl-wait-timeout（seconds）`-等待好时机多长时间。默认值为0，不等待。
- `--ftwrl-wait-query-type `-运行`FLUSH TABLES WITH READ LOCK`之前应完成哪些长时间的查询。默认为全部。
- `--ftwrl-wait-threshold（seconds）`-查询应该运行多长时间，然后我们才考虑它是长时间运行的和潜在的全局锁定阻止者。
- `--kill-long-queries-timeout（seconds）`-在开始杀死之前，发出 `FLUSH TABLES WITH READ LOCK` 后，我们给查询提供多少时间来完成。默认值为0，不杀死。
- `--kill-long-query-type` -杀死kill-long-queries-timeout后，应终止哪些查询


### 例子

使用以下选项运行 xtrabackup 将导致 xtrabackup 花费不超过3分钟的时间来等待所有超过40秒的查询完成。

```bash
xtrabackup --backup --ftwrl-wait-threshold=40 \
--ftwrl-wait-query-type=all --ftwrl-wait-timeout=180 \
--kill-long-queries-timeout=20 --kill-long-query-type=all \
--target-dir=/data/backups/
```

发出 `FLUSH TABLES WITH READ LOCK`后，xtrabackup将等待20秒以获取锁。如果20秒后仍未获得锁定，它将杀死所有运行时间超过`FLUSH TABLES WITH READ LOCK`的查询。










