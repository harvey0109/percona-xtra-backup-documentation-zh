# 流式备份

Percona XtraBackup 支持的流模式将备份以 xbstream 格式发送到 STDOUT，而不是将文件复制到备份目录。

这使您可以使用其他程序来过滤备份的输出，从而为存储备份提供更大的灵活性。例如，通过将输出管道传输到压缩实用程序来实现压缩。流备份和使用Unix管道的好处之一是可以自动加密备份。

要使用流功能，必须使用 --stream ，提供流的格式(xbstream)以及临时文件的存储位置：

```
$ xtrabackup --stream=xbstream --target-dir=/tmp
```

xtrabackup 使用 xbstream 以特殊的 xbstream 格式将所有数据文件流式传输到STDOUT。完成将所有数据文件传输到STDOUT后，它将停止 xtrabackup 并传输已保存的日志文件。

参见

More information about [xbstream](https://www.percona.com/doc/percona-xtrabackup/LATEST/glossary.html#term-xbstream)
[The xbstream binary](https://www.percona.com/doc/percona-xtrabackup/LATEST/xbstream/xbstream.html#xbstream-binary)

启用压缩后，xtrabackup 使用指定的压缩算法压缩所有输出数据，但未压缩的 meta 和 non-InnoDB 文件除外。当前唯一支持的算法是 `quicklz` 。生成的文件具有 qpress 存档格式，即 xtrabackup 生成的每个 `*.qp` 文件本质上都是一个文件的 qpress 存档，并且可以从 qcon 文件存档器中提取和解压缩，该存档器可从 Percona Software 存储库中获得。

使用 xbstream 作为流选项，可以并行复制和压缩备份，这可以大大加快备份过程。如果备份既经过压缩又经过加密，则需要先对其解密，然后再将其解压缩。


将备份流式传输到名为backup.xbstream的存档中

```
xtrabackup --backup --stream=xbstream --target-dir=./ > backup.xbstream
```

将备份流式传输到名为backup.xbstream的压缩存档中

```
xtrabackup --backup --stream=xbstream --compress --target-dir=./ > backup.xbstream
```

加密备份

```
xtrabackup –backup –stream=xbstream ./ > backup.xbstream gzip - | openssl des3 -salt -k “password” > backup.xbstream.gz.des3
```

将备份解压缩到当前目录

```
xbstream -x <  backup.xbstream
```

将压缩后的备份直接发送到另一台主机并解压缩

```
xtrabackup --backup --compress --stream=xbstream --target-dir=./ | ssh user@otherhost "xbstream -x"
```

使用netcat将备份发送到另一台服务器。

```bash
# 在目标主机上：
$ nc -l 9999 | cat - > /data/backups/backup.xbstream

# 在源主机上：
$ xtrabackup --backup --stream=xbstream ./ | nc desthost 9999
```

使用单线将备份发送到另一台服务器：

```
$ ssh user@desthost “( nc -l 9999 > /data/backups/backup.xbstream & )” && xtrabackup –backup –stream=xbstream ./ | nc desthost 9999
```

使用管道查看器工具将吞吐量降低到 `10MB/s` [1]

```
$ xtrabackup –backup –stream=xbstream ./ | pv -q -L10m ssh user@desthost “cat - > /data/backups/backup.xbstream”
```

在流传输期间对备份进行校验和：


```
# 在目标主机上：
$ nc -l 9999 | tee >(sha1sum > destination_checksum) > /data/backups/backup.xbstream
# 在源主机上：
$ xtrabackup --backup --stream=xbstream ./ | tee >(sha1sum > source_checksum) | nc desthost 9999
```

比较源主机上的校验和：

```
$ cat source_checksum
65e4f916a49c1f216e0887ce54cf59bf3934dbad  -
```

比较目标主机上的校验和：

```
$ cat destination_checksum
65e4f916a49c1f216e0887ce54cf59bf3934dbad  -
```

并行压缩与并行复制备份

```
xtrabackup --backup --compress --compress-threads=8 --stream=xbstream --parallel=4 --target-dir=./ > backup.xbstream
```

脚注

[1]从官方网站或发行包中安装(`apt install pv`)

请注意，在恢复之前需要准备好流式备份。流媒体模式不准备备份。
