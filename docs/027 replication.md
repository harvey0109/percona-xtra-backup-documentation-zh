# 在复制环境中进行备份

有特定于从复制副本进行备份的选项。

## --slave-info

备份复制副本服务器时，此选项很有用。它显示源服务器的二进制日志位置和名称。还将此信息作为CHANGE MASTER语句写入xtrabackup_slave_info文件。

此选项对于为此源设置新副本很有用。您可以使用此备份启动副本服务器，然后发出保存在 xtrabackup_slave_info 文件中的语句。有关此过程的更多详细信息，请参见如何使用Percona XtraBackup通过6个简单步骤设置复制副本。

## --safe-slave-backup

为了确保一致的复制状态，此选项将停止复制SQL线程并等待开始备份，直到 SHOW STATUS 中的 Slave_open_temp_tables 为零为止。如果没有打开的临时表，将进行备份，否则将启动和停止SQL线程，直到没有打开的临时表。如果在 --safe-slave-backup-timeout秒（默认为300秒）后 Slave_open_temp_tables 不为零，则备份将失败。备份完成后，复制SQL线程将重新启动。

从副本服务器进行备份时，始终建议使用此选项。

::: tip Warning
在使用副本作为备份源之前，请确保您的副本是源的真实副本。验证副本的一个很好的工具是 pt-table-checksum 。
:::

