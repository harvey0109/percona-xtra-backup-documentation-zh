# 恢复单个表

使用Percona XtraBackup，您可以从任何InnoDB数据库中导出单个表，然后将它们导入到具有 XtraDB 或 MySQL 8.0 的MySQL的Percona Server中。 （源不必是XtraDB或MySQL 8.0，但目的地必须是XtraDB。）这仅适用于单个.ibd文件，并且不能导出其自身.ibd文件中未包含的表。

让我们看看如何导出和导入下表：

```
CREATE TABLE export_test (
a int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
```

## 导出表

该表应该以 innodb_file_per_table 模式创建，因此，像往常一样使用 --backup 进行备份后，.ibd文件应该存在于目标目录中：

```
$ find /data/backups/mysql/ -name export_test.*
/data/backups/mysql/test/export_test.ibd
```

准备备份时，将额外的参数 --export 添加到命令中。这是一个例子：

```
$ xtrabackup --prepare --export --target-dir=/data/backups/mysql/
```

::: tip Note
如果您要还原加密的InnoDB表空间表，则还需要指定密钥环文件：
:::

```
$ xtrabackup --prepare --export --target-dir=/tmp/table \
--keyring-file-data=/var/lib/mysql-keyring/keyring
```

现在，您应该在目标目录中看到一个 `.exp` 文件：

```
$ find /data/backups/mysql/ -name export_test.*
/data/backups/mysql/test/export_test.exp
/data/backups/mysql/test/export_test.ibd
/data/backups/mysql/test/export_test.cfg
```

这三个文件都是将表导入到运行 Percona Server for MySQL（带有XtraDB或MySQL 8.0）的服务器所需的全部文件。如果服务器使用 InnoDB 表空间加密，则将为加密表列出其他 `.cfp` 文件。

::: tip Note
MySQL使用`.cfg`文件，其中包含特殊格式的InnoDB字典转储。这种格式不同于XtraDB中出于相同目的使用的`.exp`格式。严格来说，将表空间导入MySQL 8.0或Percona Server for MySQL 8.0不需要`.cfg`文件。即使一个表空间是从另一台服务器导入的，它也将被成功导入，但是，如果在同一目录中存在相应的`.cfg`文件，则InnoDB会进行模式验证。
:::

## 导入表格

在启用了 XtraDB 和 innodb_import_table_from_xtrabackup 选项的运行Percona Server for MySQL的目标服务器上，或在MySQL 8.0上，创建具有相同结构的表，然后执行以下步骤：

运行 `ALTER TABLE test.export_test DISCARD TABLESPACE` ;命令。如果看到此错误，则必须启用 `innodb_file_per_table` 并再次创建表。

```
Error

ERROR 1030 (HY000): Got error -1 from storage engine
```

将导出的文件复制到目标服务器的数据目录的 `test/` 子目录中

运行 `ALTER TABLE test.export_test IMPORT TABLESPACE`;

现在应该导入该表，并且您应该可以从中进行选择并查看导入的数据。
