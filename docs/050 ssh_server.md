# 安装和配置SSH服务器

许多Linux发行版默认只安装ssh客户端。如果你还没有安装ssh服务器，最简单的方法是使用你的发行版的打包系统来完成。

ubuntu

```
sudo apt-get install openssh-server
```

archlinux

```
sudo pacman -S openssh
```

如果你以前没有做过，你可能需要看看你的发行版的文档或在互联网上搜索一个教程来配置它。

它们的一些链接是：

- Debian - http://wiki.debian.org/SSH
- Ubuntu - https://help.ubuntu.com/10.04/serverguide/C/openssh-server.html
- Archlinux - https://wiki.archlinux.org/index.php/SSH
- Fedora - http://docs.fedoraproject.org/en-US/Fedora/12/html/Deployment_Guide/s1-openssh-server-config.html
- CentOS - http://www.centos.org/docs/5/html/Deployment_Guide-en-US/s1-openssh-server-config.html
- RHEL - http://docs.redhat.com/docs/en-US/Red_Hat_Enterprise_Linux/6/html/Deployment_Guide/ch-OpenSSH.html

