# 加快备份过程



## 使用 `--parallel` 和 `--compress-threads` 选项进行复制

使用 xbstream 选项进行本地或流式备份时，使用 --parallel 选项可以同时复制多个文件。此选项指定 xtrabackup 创建的用于复制数据文件的线程数。

要利用此选项，必须启用多个表空间选项（`innodb_file_per_table`），或者必须使用`innodb_data_file_path`选项将共享表空间存储在多个ibdata文件中。为数据库拥有多个文件（或将文件分割成多个文件）不会对性能产生可衡量的影响。

由于此功能是在文件级别实现的，因此在对高度分散的数据文件进行备份时，由于大量随机读取请求的重叠，并发文件传输有时可以提高 I/ O 吞吐量。您还应该考虑调整文件系统以获得最佳性能（例如检查碎片）。

如果数据存储在单个文件中，则此选项无效。

要使用此功能，只需将选项添加到本地备份中，例如：

```
xtrabackup --backup --parallel=4 --target-dir=/path/to/backup
```

通过在流式备份中使用 xbstream ，还可以使用 --compress-threads 选项来加快压缩过程。此选项指定 xtrabackup 创建的用于并行数据压缩的线程数。此选项的默认值为1。

要使用此功能，只需将选项添加到本地备份中，例如：


```
xtrabackup --backup --stream=xbstream --compress --compress-threads=4 --target-dir=./ > backup.xbstream
```

在应用日志之前，压缩文件将需要解压缩。


## --rsync选项

为了加快备份过程并最大程度地减少 `FLUSH TABLES WITH READ LOCK` 阻止写入的时间，应使用 --rsync 选项。指定此选项后，xtrabackup 将使用 rsync 复制所有非InnoDB文件，而不是为每个文件生成单独的cp，这对于具有大量数据库或表的服务器而言可以更快。 xtrabackup 将调用 rsync 两次，一次是在`FLUSH TABLES WITH READ LOCK`之前，一次是在此期间，以最小化保持读取锁的时间。在第二次rsync调用期间，自第一次调用在`FLUSH TABLES WITH READ LOCK`之前执行以来，它将仅将更改同步到非事务性数据（如果有）。请注意，Percona XtraBackup 将使用备份锁，这些锁可作为`FLUSH TABLES WITH READ LOCK`的轻型替代方案。 Percona Server for MySQL 5.6+中提供了此功能。 Percona XtraBackup 使用此功能自动复制非InnoDB数据，以避免阻止修改InnoDB表的DML查询。

::: tip Note
此选项不能与--stream选项一起使用。
:::







