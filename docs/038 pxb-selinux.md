# 使用SELinux

Percona XtraBackup被安装为一个非封闭的进程，运行在一个未定义的域中。SELinux允许非限制性进程几乎所有的访问，而且这些进程只使用酌情访问控制（DAC）规则。

您可以使用以下命令找到Percona XtraBackup文件的当前状态：

```
$ ls -Z /usr/bin | grep xtrabackup
-rwxr-xr-x. root root   system_u:object_r:bin_t:s0       xtrabackup
```

SELinux上下文如下：

- user (root)
- role (object_r)
- type (bin_t)
- level (s0)

非封闭域支持面向网络的服务，这些服务受到SELinux的保护。这些域是不暴露的。在这种配置中，SELinux保护远程入侵，但需要本地访问的本地入侵则不被限制。

Percona XtraBackup在本地工作。该服务不是面向网络的，不能被外部利用。该服务只与提供参数的本地用户互动。Percona XtraBackup需要访问target-dir位置。

## 限制XtraBackup

你可以修改你的安全配置来限制Percona XtraBackup。第一个问题是将备份文件存放在哪里。该服务需要对选定的位置进行读写访问。

您可以使用以下两种方法之一：

- 允许Percona XtraBackup写到任何位置。用户为target-dir参数提供任何路径。
- 允许Percona XtraBackup写到一个特定的位置，如/backups或用户的主目录。

第一个选项是打开整个系统进行读写。选择第二个选项来加强你的安全。

## 安装SELinux工具

要使用策略，必须安装SELinux工具。要查找提供semanage命令的软件包并安装该软件包。以下是CentOS 7上的示例。

```
$ yum provides *bin/semanage
...
policycoreutils-python-2.5-34.el7.x86_64 : SELinux policy core python utilities
...
$ sudo yum install -y policycoreutils-python
```

以下是CentOS 8上的示例：

```
$ yum provides *bin/semanage
...
policycoreutils-python-utils-2.8-16.1.el8.noarch : SELinux policy core python utilities
...
$ sudo yum install -y policycoreutils-python-utils
```

## 建立策略

使用模块化的方法来创建SELinux策略。创建一个策略模块来管理XtraBackup。你必须创建一个.te文件用于类型执行，以及一个可选的.fc文件用于文件上下文。

使用ps -efZ | grep xtrabackup来验证该服务没有受到SELinux的限制。

创建xtrabackup.fc文件并添加内容。这个文件定义了安全上下文。

```
/usr/bin/xtrabackup    -- gen_context(system_u:object_r:xtrabackup_exec_t,s0)
/usr/bin/xbcrypt    -- gen_context(system_u:object_r:xtrabackup_exec_t,s0)
/usr/bin/xbstream    -- gen_context(system_u:object_r:xtrabackup_exec_t,s0)
/usr/bin/xbcloud    -- gen_context(system_u:object_r:xtrabackup_exec_t,s0)
/backups(/.*)?       system_u:object_r:xtrabackup_data_t:s0
```

注意
如果你使用的是/backups目录，你必须有最后一行。如果你将备份存储在用户的主目录下，你可以省略这一行。

符合策略模块：

```
$ make -f /usr/share/selinux/devel/Makefile xtrabackup.pp
```

安装模块：

```
$ semodule -i xtrabackup.pp
```

用适当的SELinux标签标记PXB二进制文件，例如xtrabackup_exec_t。

```
$ restorecon -v /usr/bin/*
```

如果你把备份存放在/backups，就在那个位置恢复标签。

```
$ restorecon -v /backups
```

注意
记住要为这个目录添加标准的Linux DAC权限。

以标准方式执行备份。