# Percona XtraBackup的 原理

Percona XtraBackup基于InnoDB的崩溃恢复功能。它会复制您的InnoDB数据文件 , 从而导致内部数据不一致；但随后它将对文件执行崩溃恢复 , 以使它们再次成为一致 , 可用的数据库


之所以可行 , 是因为InnoDB维护了一个重做日志 , 也称为事务日志。这包含对InnoDB数据的每次更改的记录。当InnoDB启动时 , 它将检查数据文件和事务日志 , 并执行两个步骤。它将已提交的事务日志条目应用于数据文件 , 并对已修改数据但未提交的所有事务执行撤消操作。

Percona XtraBackup的工作方式是在启动时记住日志序列号（LSN） , 然后复制掉数据文件。这需要一些时间 , 因此 , 如果文件正在更改 , 则它们会在不同的时间点反映数据库的状态。同时 , Percona XtraBackup运行一个后台进程 , 该进程监视事务日志文件 , 并从中复制更改。 Percona XtraBackup需要连续执行此操作 , 因为事务日志以循环方式编写 , 并且可以在一段时间后重新使用。自开始执行以来 , Percona XtraBackup需要对数据文件的每次更改都具有事务日志记录。

Percona XtraBackup在可能的情况下使用备份锁来代替带`FLUSH TABLES WITH READ LOCK`。 Percona Server for MySQL 5.6+中提供了此功能。 MySQL 8.0允许通过`LOCK INSTANCE FOR BACKUP`语句获取实例级备份锁。

在Percona XtraBackup完成备份所有InnoDB / XtraDB数据和日志之后 , 仅对MyISAM和其他非InnoDB表执行锁定。 Percona XtraBackup使用此功能自动复制非InnoDB数据 , 以避免阻止修改InnoDB表的DML查询。

::: tip 重要
要有效地使用 `LOCK INSTANCE FOR BACKUP` 或 `LOCK TABLES FOR BACKUP` , 需要 `BACKUP_ADMIN` 特权才能查询 `performance_schema.log_status`。
:::

当实例仅包含InnoDB表时 , xtrabackup会尝试避免备份锁和`FLUSH TABLES WITH READ LOCK`。在这种情况下 , xtrabackup从performance_schema.log_status获取二进制日志坐标。当使用--slave-info启动xtrabackup时 , 在MySQL 8.0中仍需要使用`FLUSH TABLES WITH READ LOCK`。 Percona Server for MySQL 8.0中的log_status表已扩展为包括中继日志坐标 , 因此即使使用--slave-info选项也不需要锁。

参见

[MySQL文档：有关 `LOCK INSTANCE FOR BACKUP`的更多信息](https://dev.mysql.com/doc/refman/8.0/en/lock-instance-for-backup.html)

当服务器支持备份锁时 , xtrabackup首先复制InnoDB数据 , 运行 `LOCK TABLES FOR BACKUP` , 然后复制MyISAM表。完成此操作后 , 将开始文件备份。它将备份`.frm` , .MRG , `.MYD` , `.MYI` , .ARM , `.ARZ` , `.CSM` , `.CSV` , `.sdi`和`.par`文件。

之后 , xtrabackup将使用`LOCK BINLOG FOR BACKUP`来阻止所有可能更改二进制日志位置或Exec_Master_Log_Pos或Exec_Gtid_Set的操作（例如 , 与复制副本上的当前SQL线程状态相对应的源二进制日志坐标） , 如`SHOW MASTER / SLAVE STATUS`所报告的那样。然后xtrabackup将完成复制REDO日志文件并获取二进制日志坐标。完成此操作后 , xtrabackup将解锁二进制日志和表。

最后 , 二进制日志位置将被打印到STDERR , 如果一切正常 , xtrabackup将退出并返回0。

请注意 , xtrabackup的STDERR未写入任何文件中。您将不得不将其重定向到文件 , 例如 `xtrabackup OPTIONS 2> backupout.log`。

它还将在备份目录中创建[以下文件](https://www.percona.com/doc/percona-xtrabackup/LATEST/xtrabackup-files.html#xtrabackup-files)。

在准备阶段 , Percona XtraBackup使用复制的事务日志文件对复制的数据文件执行崩溃恢复。完成此操作后 , 就可以还原和使用数据库了。


备份的MyISAM表和InnoDB表最终将彼此一致 , 因为在准备（恢复）过程之后 , InnoDB的数据将前滚到备份完成的点 , 而不是回滚到备份开始的点。该时间点与采取带读取锁定的刷新表的位置匹配 , 因此MyISAM数据和准备好的InnoDB数据是同步的。


xtrabackup提供了前面解释中未提及的许多功能。本手册将进一步详细说明每种工具的功能。简而言之 , 借助这些工具 , 您可以通过复制数据文件 , 复制日志文件以及将日志应用于数据的各种组合来执行诸如流式备份和增量备份之类的操作。

## 还原备份

要使用xtrabackup恢复备份 , 可以使用`--copy-bac`k或`--move-back`选项。

xtrabackup将从`my.cnf`中读取变量 `datadir` , `innodb_data_home_dir` , `innodb_data_file_path` , `innodb_log_group_home_dir` , 并检查目录是否存在。

它将首先复制MyISAM表 , 索引等（`.MRG` , `.MYD` , `.MYI` , `.ARM` , `.ARZ` , `.CSM `, `.CSV` , `.sdi`和`.par`文件） , 然后复制InnoDB表和索引以及日志文件。复制文件时 , 它将保留文件的属性 , 在启动数据库服务器之前 , 您可能必须将文件的所有权更改为mysql , 因为它们将由创建备份的用户拥有。

或者 , 可以使用`--move-back`选项还原备份。此选项与`--copy-back`相似 , 唯一的区别是 , 不是复制文件 , 而是将文件移动到目标位置。由于此选项将删除备份文件 , 因此必须谨慎使用。在没有足够的可用磁盘空间来容纳数据文件及其备份副本的情况下 , 此选项很有用。
