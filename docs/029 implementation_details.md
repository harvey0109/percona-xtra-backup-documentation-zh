# 执行细节

此页面包含有关xtrabackup工具操作的各个内部方面的注释。



## 文件权限

xtrabackup以读写模式打开源数据文件，尽管它不会修改文件。这意味着您必须以具有写入数据文件权限的用户身份运行xtrabackup。以读写模式打开文件的原因是xtrabackup使用嵌入式的InnoDB库来打开和读取文件，而InnoDB以读写模式打开它们是因为它通常假定要写入文件。

## 调整操作系统缓冲区

因为xtrabackup从文件系统读取大量数据，所以它在可能的情况下使用`posix_fadvise（）`来指示操作系统不要尝试缓存从磁盘读取的块。如果没有此提示，则操作系统将更喜欢缓存这些块，假设xtrabackup可能再次需要它们，事实并非如此。缓存如此大的文件可能会对操作系统的虚拟内存造成压力，并导致其他进程（例如数据库服务器）被换出。 xtrabackup工具通过在源文件和目标文件上给出以下提示来避免这种情况：

```
posix_fadvise(file, 0, 0, POSIX_FADV_DONTNEED)
```

此外，xtrabackup要求操作系统对源文件进行更积极的先读优化。

```
posix_fadvise(file, 0, 0, POSIX_FADV_SEQUENTIAL)
```

## 复制数据文件

将数据文件复制到目标目录时，xtrabackup一次读取和写入1 MB数据。这是不可配置的。复制日志文件时，xtrabackup一次读取和写入512个字节。这也是无法配置的，并且与InnoDB的行为相匹配（Percona Server for MySQL中存在解决方法，因为它可以为XtraDB调整innodb_log_block_size，并且在这种情况下，Percona XtraBackup将与调整相匹配）。

从文件中读取后，xtrabackup一次遍历1MB缓冲区一次页面，并使用InnoDB的buf_page_is_corrupted（）函数检查每个页面上的页面是否损坏。如果页面损坏，则每个页面最多重新读取和重试10次。它将跳过对doublewrite缓冲区的检查。

