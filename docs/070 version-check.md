# Version Checking


Some Percona software contains “version checking” functionality which is a
feature that enables Percona software users to be notified of available software
updates to improve your environment security and performance. Alongside this,
the version check functionality also provides Percona with information relating
to which software versions you are running, coupled with the environment
confirmation which the software is running within. This helps enable Percona to
focus our development effort accordingly based on trends within our customer
community.

The purpose of this document is to articulate the information that is collected,
as well as to provide guidance on how to disable this functionality if desired.



## Usage[¶](#usage 'Permalink to this headline')


*Version Check* was implemented in Percona Toolkit 2.1.4, and was enabled by default in
version 2.2.1. Currently it is supported as a `<span class="pre">--[no]version-check</span>` option
by [a number of tools in Percona Toolkit](https://www.percona.com/doc/percona-toolkit/LATEST/genindex.html),
Percona XtraBackup, and PMM (Percona Monitoring and Management).

When launched with Version Check enabled, the tool that supports this feature
connects to a Percona’s *version check service* via a secure HTTPS channel. It
compares the locally installed version for possible updates, and also checks
versions of the following software:


- Operating System
- Percona Monitoring and Management (PMM)
- MySQL
- Perl
- MySQL driver for Perl (DBD::mysql)
- Percona Toolkit

Then it checks for and warns about versions with known problems if they are
identified as running in the environment.

Each version check request is logged by the server. Stored information consists
of the checked system unique ID followed by the software name and version.  The
ID is generated either at installation or when the *version checking* query is
submitted for the first time.



Note

Prior to version 3.0.7 of Percona Toolkit, the system ID was calculated as an MD5 hash
of a hostname, and starting from Percona Toolkit 3.0.7 it is generated as an MD5 hash of
a random number. Percona XtraBackup continues to use hostname\-based MD5 hash.



As a result, the content of the sent query is as follows:

<pre><span></span><span class="mi">85624</span><span class="n">f3fb5d2af8816178ea1493ed41a</span><span class="p">;</span><span class="n">DBD</span><span class="p">::</span><span class="n">mysql</span><span class="p">;</span><span class="mf">4.044</span>
<span class="n">c2b6d625ef3409164cbf8af4985c48d3</span><span class="p">;</span><span class="n">MySQL</span><span class="p">;</span><span class="n">MySQL</span> <span class="n">Community</span> <span class="n">Server</span> <span class="p">(</span><span class="n">GPL</span><span class="p">)</span> <span class="mf">5.7</span><span class="o">.</span><span class="mi">22</span><span class="o">-</span><span class="n">log</span>
<span class="mi">85624</span><span class="n">f3fb5d2af8816178ea1493ed41a</span><span class="p">;</span><span class="n">OS</span><span class="p">;</span><span class="n">Manjaro</span> <span class="n">Linux</span>
<span class="mi">85624</span><span class="n">f3fb5d2af8816178ea1493ed41a</span><span class="p">;</span><span class="n">Percona</span><span class="p">::</span><span class="n">Toolkit</span><span class="p">;</span><span class="mf">3.0</span><span class="o">.</span><span class="mi">11</span><span class="o">-</span><span class="n">dev</span>
<span class="mi">85624</span><span class="n">f3fb5d2af8816178ea1493ed41a</span><span class="p">;</span><span class="n">Perl</span><span class="p">;</span><span class="mf">5.26</span><span class="o">.</span><span class="mi">2</span>
</pre>







## Disabling Version Check[¶](#disabling-version-check 'Permalink to this headline')


Although the *version checking* feature does not collect any personal information,
you might prefer to disable this feature, either one time or permanently.  To
disable it one time, use `<span class="pre">--no-version-check</span>` option when invoking the tool
from a Percona product which supports it. Here is a simple example which shows
running [pt-diskstats](https://www.percona.com/doc/percona-toolkit/LATEST/pt-diskstats.html) tool
from the Percona Toolkit with *version checking* turned off:

<pre><span></span><span class="n">pt</span><span class="o">-</span><span class="n">diskstats</span> <span class="o">--</span><span class="n">no</span><span class="o">-</span><span class="n">version</span><span class="o">-</span><span class="n">check</span>
</pre>



Disabling *version checking* permanently can be done by placing
`<span class="pre">no-version-check</span>` option into the configuration file of a Percona product
(see correspondent documentation for exact file name and syntax). For example,
in case of Percona Toolkit [this can be done](https://www.percona.com/doc/percona-toolkit/LATEST/configuration_files.html)
in a global configuration file `<span class="pre">/etc/percona-toolkit/percona-toolkit.conf</span>`:

<pre><span></span><span class="c1"># Disable Version Check for all tools:</span>
<span class="n">no</span><span class="o">-</span><span class="n">version</span><span class="o">-</span><span class="n">check</span>
</pre>



In case of Percona XtraBackup this can be done [in its configuration file](https://www.percona.com/doc/percona-xtrabackup/2.4/using_xtrabackup/configuring.htm)
in a similar way:

<pre><span></span><span class="p">[</span><span class="n">xtrabackup</span><span class="p">]</span>
<span class="n">no</span><span class="o">-</span><span class="n">version</span><span class="o">-</span><span class="n">check</span>
</pre>







## Frequently Asked Questions[¶](#frequently-asked-questions 'Permalink to this headline')





- [Why is this functionality enabled by default?](#why-is-this-functionality-enabled-by-default)
- [Why not rely on Operating System’s built in functionality for software updates?](#why-not-rely-on-operating-system-s-built-in-functionality-for-software-updates)
- [Why do you send more information than just the version of software being run as a part of version check service?](#why-do-you-send-more-information-than-just-the-version-of-software-being-run-as-a-part-of-version-check-service)





### [Why is this functionality enabled by default?](#id1)[¶](#why-is-this-functionality-enabled-by-default 'Permalink to this headline')


We believe having this functionality enabled improves security and performance
of environments running Percona Software and it is good choice for majority of
the users.





### [Why not rely on Operating System’s built in functionality for software updates?](#id2)[¶](#why-not-rely-on-operating-system-s-built-in-functionality-for-software-updates 'Permalink to this headline')


In many environments the Operating Systems repositories may not carry the latest
version of software and newer versions of software often installed manually, so
not being covered by operating system wide check for updates.





### [Why do you send more information than just the version of software being run as a part of version check service?](#id3)[¶](#why-do-you-send-more-information-than-just-the-version-of-software-being-run-as-a-part-of-version-check-service 'Permalink to this headline')


Compatibility problems can be caused by versions of various components in the
environment, for example problematic versions of Perl, DBD or MySQL could cause
operational problems with Percona Toolkit.