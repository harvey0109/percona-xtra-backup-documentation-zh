# 在 Red Hat Enterprise Linux 和 CentOS 上安装 Percona XtraBackup


现成的软件包可以从Percona XtraBackup软件仓库和[下载页面](https://www.percona.com/downloads/XtraBackup/)获得。Percona yum软件库支持流行的基于RPM的操作系统，包括Amazon Linux AMI。

安装Percona Yum资源库的最简单方法是安装一个配置yum和安装[Percona GPG key.](https://www.percona.com/downloads/RPM-GPG-KEY-percona)RPM。

Specific information on the supported platforms, products, and versions is described in [Percona Software and Platform Lifecycle](https://www.percona.com/services/policies/percona-software-platform-lifecycle#mysql).

关于支持的平台、产品和版本的具体信息在[ Percona Software and Platform Lifecycle.](https://www.percona.com/services/policies/percona-software-platform-lifecycle#mysql)中描述。

## 每个RPM包里有什么？

`percona-xtrabackup-80`软件包包含最新的Percona XtraBackup GA二进制文件和相关文件。



| Package         | Contains                  |
| :-------------------------------- | :--------------- |
| `percona-xtrabackup-80-debuginfo` | `percona-xtrabackup-80`中二进制文件的调试符号 |
| `percona-xtrabackup-test-80`      | Percona XtraBackup的测试套件|
| `percona-xtrabackup`              | 旧版本的Percona XtraBackup |



## 从Percona yum资源库安装Percona XtraBackup



通过以root用户或sudo运行以下命令来安装Percona yum资源库：`yum install https://repo.percona.com/yum/percona-release-latest.noarch.rpm`

启用版本库： percona-release enable-only tools release

如果Percona XtraBackup打算与上游的MySQL服务器结合使用，你只需要启用工具库：`percona-release enable-only tools`。

通过运行：`yum install percona-xtrabackup-80`来安装Percona XtraBackup。

::: tip Warning

在CentOS 6上安装Percona XtraBackup之前，请确保你已经安装了`libev`包。对于这个操作系统，`libev` 包可以从 [EPEL](https://fedoraproject.org/wiki/EPEL) 仓库中获得。
:::

为了能够进行压缩备份，安装 `qpress` 软件包。

```bash
yum install qpress
```



See also [Compressed Backup](https://www.percona.com/doc/percona-xtrabackup/LATEST/backup_scenarios/compressed_backup.html#compressed-backup)

## 使用下载的rpm包安装Percona XtraBackup

Download the packages of the desired series for your architecture from the [download page]. The following example downloads *Percona XtraBackup* 8.0.4 release package for *CentOS* 7:

从[下载页面](https://www.percona.com/downloads/XtraBackup/)下载适合你的架构的所需系列的软件包。下面的例子是为CentOS 7下载Percona XtraBackup 8.0.4发布包。


```bash
wget https://www.percona.com/downloads/XtraBackup/Percona-XtraBackup-8.0.4/binary/redhat/7/x86_64/percona-xtrabackup-80-8.0.4-1.el7.x86_64.rpm
```

现在你可以通过运行`yum localinstall`来安装Percona XtraBackup。

```bash
yum localinstall percona-xtrabackup-80-8.0.4-1.el7.x86_64.rpm
```

::: tip 笔记
像这样手动安装软件包时，你需要确保解决所有的依赖关系，并自己安装丢失的软件包。
:::

## 卸载Percona XtraBackup

要完全卸载Percona XtraBackup，你需要删除所有安装的软件包： `yum remove percona-xtrabackup`
