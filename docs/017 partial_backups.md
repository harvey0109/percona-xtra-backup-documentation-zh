# 部分备份

启用 innodb_file_per_table 选项时，xtrabackup 支持进行部分备份。有三种创建部分备份的方法：

- 将表名称与正则表达式匹配
- 在文件中提供表名列表
- 提供数据库列表

::: tip Warning
不要把准备好的备份复制回去
还原部分备份应通过导入表来完成，而不要使用 `--copy-back` 选项。不建议在运行部分备份之后运行增量备份。
尽管在某些情况下可以通过复制回文件来完成还原，但是在许多情况下，这可能会导致数据库不一致，因此不建议这样做。
:::

在本手册页中，我们假设有一个名为 test 的数据库，其中包含名为 t1 和 t2 的表。

::: tip Warning
如果在备份过程中删除了任何匹配或列出的表，则 xtrabackup 将失败。
:::

## 创建部分备份

有两种方法可以指定要备份整个数据的哪一部分：枚举文件中的表（--tables-file）或提供数据库列表（--database）。

## 选项 --tables

第一种方法涉及 xtrabackup --tables 选项。该选项的值是一个正则表达式，它与标准的表名（包括数据库名）匹配，格式为 databasename.tablename 。

要仅备份测试数据库中的表，可以使用以下命令：

```
$ xtrabackup --backup --datadir=/var/lib/mysql --target-dir=/data/backups/ \
--tables="^test[.].*"
```

要仅备份表test.t1，可以使用以下命令：

```
$ xtrabackup --backup --datadir=/var/lib/mysql --target-dir=/data/backups/ \
--tables="^test[.]t1"
```

## --tables-file选项

--tables-file 选项指定一个文件，该文件可以包含多个表名，文件中每行一个表名。仅备份文件中命名的表。名称完全匹配，区分大小写，没有模式或正则表达式匹配。表名必须完全合格，格式为 databasename.tablename 。

```
$ echo "mydatabase.mytable" > /tmp/tables.txt
$ xtrabackup --backup --tables-file=/tmp/tables.txt
```

## --databases 和 --databases-file 选项

xtrabackup --databases 接受以空格分隔的数据库和表列表，以格式 `databasename [.tablename]`进行备份。除了此列表之外，请确保指定 mysql，sys 和 performance_schema 数据库。使用 xtrabackup --copy-back 还原数据库时，这些数据库是必需的。

::: tip Warning
在 `--prepare` 步骤中处理的表也可以添加到备份中，即使这些参数未在备份开始后创建，也未由参数明确列出。
::: 

```
$ xtrabackup --databases='mysql sys performance_schema ...'
```

xtrabackup --databases-file 指定一个文件，该文件可以以 `databasename [.tablename]`格式包含多个数据库和表，该文件中每行一个元素名称。名称完全匹配，区分大小写，没有模式或正则表达式匹配。

::: tip Warning
在 `--prepare` 步骤中处理的表也可以添加到备份中，即使这些参数未在备份开始后创建，也未由参数明确列出。
::: 


## 准备部分备份

该过程类似于还原单个表：应用日志并使用 `--export` 选项：

```
$ xtrabackup --prepare --export --target-dir=/path/to/partial/backup
```

在部分备份上使用 xtrabackup --prepare 选项时，将看到有关不存在的表的警告。这是因为这些表存在于InnoDB的数据字典中，但是不存在相应的.ibd文件。它们没有被复制到备份目录中。这些表将从数据字典中删除，并且当您还原备份并启动InnoDB时，它们将不再存在并且不会导致任何错误或警告被打印到日志文件。

您将在准备阶段看到错误消息的示例。

```
InnoDB: Reading tablespace information from the .ibd files...
101107 22:31:30  InnoDB: Error: table 'test1/t'
InnoDB: in InnoDB data dictionary has tablespace id 6,
InnoDB: but tablespace with that id or name does not exist. It will be removed from data dictionary.
```

## 恢复部分备份

还原应通过将部分备份中的各个表还原到服务器来完成。

也可以通过将准备好的备份复制回“干净的”数据目录（在这种情况下，请确保包括mysql数据库）来完成此操作。可以使用以下方法创建系统数据库：

```
$ sudo mysql_install_db --user=mysql
```
